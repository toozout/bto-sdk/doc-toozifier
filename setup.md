# Initial Setup

## 1. Requirements

The [Guide Book](guidebook.md) describes the overall architecture of the tooz platform. Make sure you read it to ensure familiarity with the core concepts of the platform.

To use the SDK you will need the following setup:

1. An Android smartphone (Android 7+) with the latest *tooz Control App* installed (**toozOS device**).
2. The tooz Developer Kit 2020 (**tooz DevKit 20**).
3. If you do not have the glasses at hand, just use another smart mobile device (Android 7+) to emulate the tooz smart glasses (**tooz glasses**) by installing the tooz Emulator.

You can download the apps from our [Downloads](downloads.md) page.


Please note that some functions, such as the weather, require a mobile internet connection to provide the full user experience. 

## 2. Charging

Please charge the *tooz smart glasses* for one hour before using them the first time:

1.	Unfold the glasses temples 
2.	Attach charging cable on the pogo pin connector (lower side of the glasses left temple)
3.	Ensure LED flashes
4.	Once the glasses are completely charged, the charging LED stays on


<img src="images/charging.png" width="380"/>


## 3. Connecting the tooz smart glasses with a mobile device

**Step 1a:** Put tooz smart glasses into Pairing Mode

1.	Unfold temples of the glasses 
2.	Wait until the glasses finish booting
3.	On the touchpad: Swipe backwards and hold
4.	Proceed with Step 2

 <img src="images/Start-Screen.png" alt="Start Screen" width="120"/>
 <img src="images/Pairing_Mode.png" alt="Pairing Mode" width="120"/>

_Fig 1: Glasses finished booting_  
_Fig 2: Glasses are in pairing mode_
<br><br><br>
**Step 1b:** Open the tooz Emulator on a different smart mobile device (e.g. tablet or phone)

*Please note, for the tooz Emulator and tooz Control apps to work in tandem, they must be installed on separate devices.*

1.	Open the app and ensure Bluetooth is enabled
2.	Read and accept the app’s usage terms
3.	Tap on *Frame Only*
4.	Tap on *Pairing Mode*
5.	Proceed with Step 2

 <img src="images/02-Frame-Only.png" alt="Frame-Only.png" width="120"/>
 <img src="images/Enter-Pairing-Mode.png" alt="Enter Pairing Mode" width="120"/>
 <img src="images/Proceed_Step2.png" alt="Proceed with step 2" width="120"/>
 
_Fig 1: Start-Off with the Frame-Only-View_  
_Fig 2: Enter Pairing Mode_   
_Fig 3: Proceed with Step 2_
<br><br><br>
**Step 2:** Open the tooz Control App on a mobile device and connect to glasses

1.	Read and accept the app’s usage terms 
2.	Tap on the button *Connect Glasses*
3.	Search for the device you want to connect by scrolling through the stack of detected devices. 
Please note that the tooz glasses show the last for digits of the MAC Address (12:20) on the pairing mode screen; If you aim to connect to the tooz Emulator, search for the name of the mobile device.  
4.	Tap to ‘Connect’ to the glasses in the app and wait until the devices are connected

<img src="images/001-Pairing-Welcome.png" width="120"/>
<img src="images/002-Pairing-First-Results.png" width="120"/>
<img src="images/003-Pairing-StartView.png" width="120"/>
<img src="images/005-Pairing-004-Connected.png" width="138"/>

_Fig 1: If no tooz glasses are connected_  
_Fig 2: List of detected tooz glasses_   
_Fig 3: Start View of tooz Control App after connecting_   
_Fig 4: Home screen of the glasses_ 
 

**Troubleshooting**  
* If your tooz smart glasses are not found in the list of detected devices, tap on ‘Scan Again’ and wait while the app searches for more devices. Then, swipe to the front of the stack, as new detected glasses are added on the top of the stack. 
* If the devices do not connect, tap to ‘Try Again’. If the devices still do not connect, •	ensure the tooz smart glasses are in pairing mode and try again. 
* If device is not detected, pair to the device via the android Bluetooth settings. Activate ‘Pairing Mode’ and search for the device again in the stack of detected mobile devices.
* If it is not possible to tap on the button ‘Scan Again’, pair the devices via the android pairing settings or give tooz OS location rights. 

## 4. Glasses View States

There are two different views for every toozie and toozers compatible to the tooz glasses.
<br><br>
**1.) App-View**  
<img src="images/PromptView_Weather.png" width="160"/>  
After the user opens an app, the app view is presented to the user and the app can demand various user inputs e.g. voice inputs | When an app is open but not in the foreground, it can send notifications to the glasses as prompt views. The active app will not be closed when other apps send prompts.
<br><br>
**2.) Prompt-View**  
<img src="images/AppView_Weather.png" width="160"/>  
All prompt-views of the active apps are summarized in a stack. When the user performs a backward swipe on the tooz glasses the so called “Stack-View” is opened. By swiping backwards, the user can scroll through the stack and open an app. If the user arrives at the end of the stack, the first card of the stack is shown again.
<br><br>
**Stack-Views**

<img src="images/Stack1.png" width="160"/>
<img src="images/Stack2.png" width="160"/>
<img src="images/Stack3.png" width="160"/>

*Fig 1: Apps summarized into a stack. Although more than 3 apps might be active, the view only shows 3 apps*  
*Fig 2: Second last card of the stack*  
*Fig 3: After this last prompt of the stack, the first card of the stack will be shown again*

## 5. Touch gestures

Touch gestures available on the *tooz DevKit 20*:  

**Single Tap**  
Open and control the active app

<img src="images/Single-Tap.png" width="80"/>  

***Important note for developers:*** The single tap touch gesture is the only action that is forwarded to users of the Android tooz SDK since all other actions are consumed by the *tooz Control App*.

<br><br>
**Forward Swipe**   
Show home screen  
<img src="images/Forward-Swipe.png" width="80"/>  

<br><br>
**Long Tap**  
Smart Voice Command (not accessible in China)

<img src="images/Long-Tap.png" width="80"/>  

<br><br>
**Backward Swipe**  
Enter and scroll through Stack View

<img src="images/Backward-Swipe.png" width="80"/>  

<br><br>
***Special Controls***  

**Pairing Mode**  
Swipe backwards and hold  
<img src="images/Pairing-Mode.png" width="100"/>  

<br><br>
**Glasses Factory Reset**  
Swipe forward, backward and hold  
<img src="images/Factory-Reset.png" width="100"/>  



**Please note when using the tooz Emulator:**

A feature included in the tooz Emulator is the option ‘Glass View’. In this view, you can use the up and down volume buttons to control the glasses:  

| Phone volume buttons | Translated touch gesture | User Action |
| ------------- | ------------- | ------------- |
| Click Volume Down | Single Tap | Open and control app |
| Hold Volume Down | Long Tap | Voice Command (not accessible in Chinese mobile networks) |
| Click Volume Up | Backward Swipe | Enter and scroll through stack to switch app |
| Hold Volume Up | Forwad Swipe | Show home screen |
 <br><br> 

The following screenshot shows the ‘Glass View’. This view makes it possible to see how the actual screen will look with a black-to-alpha transparency, essentially emulating the tooz smart glasses.

<img src="images/Glass-View-Emulator.png" width="450"/>&nbsp;  


## 6. tooz Control App

Once the devices are connected, the companion app consists of **two main views**: 

<img src="images/Control-App-Views.png" alt="Different App Views of tooz Control App" width="500"/>

_Fig. 1: tooz Control App wallpaper_

In more detail, there are further pages to discover that serve different purposes:  

**Start-View**  
<img src="images/03-StartView.png" width="180"/>  
Use the Start View to quickly adjust the glasses screen brightness or to enter the Do-Not-Disturb-Mode (DND).
<br><br>
**Grid-View**  
<img src="images/04-GridView.png" width="180"/>  
Enter the Grid View to open, close or mute compatible apps installed on your phone.
<br><br>
**Stack-View**  
<img src="images/07-StackView.png" width="180"/>  
Tap on the card in the Grid View to enter the Stack View to scroll through the stack on the phone. Tap on the app to open it or close all apps.
<br><br>
**Settings**  
<img src="images/08-Settings.png" width="180"/>  
Personalize the experience via the different setting possibilities.  

## 7. Creating slideshows with tooz Demo Gallery toozie

<img src="images/05-DemoGallery.png" width="180"/>  

With the internal application *Demo Gallery*, image slideshows can be displayed on the glasses. This application serves two main purposes:

1.	To create and test app designs and prototypes 
2.	To demonstrate potential use cases to stakeholders  

Integrating your images in the app is easy: 

1.	Connect phone to PC (e.g. via USB-Cable) and chose the USB-Mode ‘File Transfer’
2.	Open the tooz "Demo Gallery" toozie on the phone and allow the relevant file access rights
3.	On your PC, open the phone’s local storage and navigate to the app’s folder: This PC > Devices > ‘Phone Name’ > Android > Data > com.tooztech.bto.toozos 
4.	Open the folder “DemoGallery” 
5.	If there are no subfolders available, create new folders for each of your slideshows and make sure the subfolders are named ‘A’ / ‘B’ / ‘C’ / ’D’ / ’1’ / ’2’ / ’3’ / ’4’
6.	Add your own images to each of the subfolders and ensure that the images are in the right format (400 x 640 px, portrait mode, See [tooz Design Implications Link](https://www.figma.com/file/zTSPZLIkCQqys7PwlT8mNt/tooz-App-Design-Implications-and-Examples?node-id=3%3A6559))
7.	Finally, go back to your tooz Control app on your smart device and tap through the different buttons to see your own images on the glasses

**Please note:** When updating to the new version of the app, all files and images will be kept. When the tooz Control App is uninstalled however, all files and images copied by the user into the tooz Control App directories will also be deleted and will not be restored even if the app is re-installed. This also applies to the images added to the Demo Gallery.

## 8. Disconnection

You can simply disconnect from the glasses by folding the glasses' hinges or by using _tooz Control App_: 

1. Tap on the "Settings" icon on tooz Control App Start Page
2. Tap on menu icon "Connected Devices" 
3. Tap to "Disconnect"


## 9. Extended Troubleshooting

### A) _tooz Control App_ cannot connect to the glasses

1. Close the _tooz Control App_.
2. From the notification bar, select the _tooz Control App_ service, expand it, and press the "Stop" button. This will end the foreground service that is maintaining the connectivity.
3. Try running the _tooz Control App_ again.
4. If this does not help, go to the settings on your Android phone, select Apps, find _tooz Control App_, select "Storage", and then "Clear Data".

### B) Devices are paired but not showing in _tooz Control App_ connection list

In order to reduce the list of Bluetooth devices, _tooz Control App_ only shows devices that broadcast themselves with a certain name pattern. Make sure that the Bluetooth name of your _tooz smart glasses_ starts with either "tooz" or "glasses". If you do not want to rename the Bluetooth name of the device, you can also manually pair the two devices in the Bluetooth device settings.
