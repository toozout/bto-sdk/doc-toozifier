# tooz DevKit Software Developer’s Guidebook

## 1. Introduction
This document aims to guide the developers who are interested in developing apps or new features for the tooz Developer Kit 2020 (henceforth called: "tooz DevKit"). The Guidebook provides an overview of the basic concepts, the description of the glasses, the development library, design guidelines and two examples.

### 1.1 Formatting Conventions
* Single and multiword terms and references to them, such as *tooz DevKit*, will be printed in italic.
* Code or script inline or multiline snippets will be printed in monospace like: `public static void main()`, or multiline code snippets:
```
int main() {
  return 0;
}
```

### 1.2 Target Audience
This document is aimed towards Android developers. It assumes that you can build Android applications and link third party libraries with it.


## 2. Overview of tooz DevKit
The *tooz DevKit* consists of several key concepts and components:

* Glasses: The actual hardware
* My Smart Glasses: Running on smartphone and managing apps for *tooz DevKit*
* Third-party / External apps (***toozers***): Standalone APKs which communicate with the My Smart Glasses app to control or communicate with the tooz DevKit.
* Integrated apps (***toozies***): Apps that are integrated in the My Smart Glasses app.

Additional concepts/components which are relevant:

* Emulator on mobile devices (smartphone or tablet)
* tooz SDK

Please find an overview of the *tooz DevKit* and related components in the following figure:

![](images/bto_components.png)


### 2.1 tooz DevKit

The *tooz DevKit* has a basic microprocessor and the following input and output sources:

* **Input**:
	* Touchpad for user interaction (described in the user guide)
	* An Inertial Measurement Unit (***IMU***), including a magnetometer 
	* An ambient light sensor (***ALS***) to measure the surrounding light conditions
* **Output**: 
	* Glasses’ OLED Display (***Glasses Screen***)
	* Speakers (right temple)
* **Connectivity**: Bluetooth 5.0 connectivity to companion device (Android and iOS)
* **Software**: The firmware running on the *tooz DevKit* provides the expected on-device functionalities of the glasses in addition to the communication between the glasses and the companion app. Section ‎2 provides a detailed description of the firmware and its requirements. 


### 2.3	My Smart Glasses
**_My Smart Glasses_** is a mobile application for android phones which connects to the *tooz DevKit* via Bluetooth. It acts as an external operating system to the *tooz DevKit*. It has an extensible architecture with two core components:

1. A virtual screen output for extensions and external apps to provide content to be displayed on the *tooz DevKit*
2. An event-handling mechanism for the extensions to respond to input events from the *tooz DevKit*. A detailed description of My Smart Glasses is provided in Section ‎5.


### 2.4	toozies
**_toozies_** are software extensions installed on the My Smart Glasses app that use the I/O interface of My Smart Glasses to provide functionalities on the tooz Smart Glasses. Note that toozies are software bundles within the scope of My Smart Glasses and are not visible as a separate app on the mobile device.
At any moment, the toozie that has the control of the glasses is called the ***active toozie***.

 
My Smart Glasses has three default toozies integrated:

* **"Clock"**: Displays the time.
* **"Weather"**: Shows the weather at the mobile phone's location.
* **"Demo Gallery"**: Plays image slideshows.

### 2.5	toozers (Third-Party Apps)
**_toozers_** are standard Android apps. In most cases, they are developed & provided by third-parties. Utilizing the inter-app communication mechanisms (IPC) of the mobile operating system, theses apps can detect the presence of My Smart Glasses on the mobile device. Through My Smart Glasses these apps can send screen content to the glasses or receive user input from the glasses. At any moment, the toozer that has the control of the glasses is called the ***active toozer***. 


### 2.6	Content Provider
For the sake of readability and when needed, we collectively call toozies and toozers ***Content Providers***. These instances can send content to the glasses. Please note that there can only be one single content provider at a time (one app in the foreground). This app can send content via app views (using the full screen) and receive user inputs. The other opened apps, can only send prompt-views to the user (similar to notifications on a smartphone).  

### 2.7	Emulator
The tooz Emulator on the mobile device ***EOM*** is an Android app running on a standard Android mobile device. It act as an emulator of the *tooz DevKit*. The app also enables to perform future touch events, so that the phone can act as a complete stand-in of the *tooz DevKit*.

## 3	Development Concepts
_My Smart Glasses_ acts as an operating system for _tooz DevKit_ (or _Emulator_). The tooz Glasses act as an output device (display) and input device (touchpad events, IMU input, etc.) connected to My Smart Glasses. 

The My Smart Glasses app provides an abstraction layer between _tooz DevKit_ (or _Emulator_) and the _Content Providers_ (_toozers_ / _toozies_). 
 
There are two main scenarios of communication between _tooz DevKit_ (or _Emulator_) and the _Content Providers_:

* _tooz DevKit_ (or _Emulator_) sends input (button clicks, audio chunks, IMU data) to _Content Providers_ through _My Smart Glasses_
*	_Content Providers_ process input updates and/or generate and send next UI frame to _tooz DevKit_ (or _Emulator_), through _My Smart Glasses_.

These communication scenarios are performed through a Bluetooth connection with a proprietary protocol which is implemented by _My Smart Glasses_ and _tooz DevKit_ (or _Emulator_). The software concept map of the interaction is shown below.

![](images/sw_components.png)


## 4	Toozifier Library (tooz SDK)
We provide toozer developers a library that abstracts away glasses connectivity and its implementation details. The library provides an interface to register for _tooz DevKit’_ state, provide content for the screen, and receive button events, audio events and IMU data. All inputs are provided through Java/Kotlin objects with specific interfaces and UI frames are sent as objects with specific interfaces, without the need to worry about actual transport layers.


## 5 Glossary

Term | Explanation
--- | --- 
Active toozie/toozer | The toozie or toozer that has the control of the glasses
ALS |	Ambient Light Sensor
tooz DevKit |	tooz technologies’ Bluetooth-only smart glasses
Glasses screen | visible screen of tooz DevKit / Emulator App
Content Provider | Any toozie or toozer, providing content to the glasses
Emulator | Android app acting as an emulator of the tooz DevKit
EOM | Emulator app running on the mobile device
FTA | Firmware Test Application 
IMU | Inertial Measurement Unit
MUC | Microprocessor
My Smart Glasses | A mobile application that connects to the tooz DevKit over Bluetooth and acts as an external operating system to the tooz DevKit.
My Smart Glasses Service | The part of My Smart Glasses that runs as a service on the mobile device.
My Smart Glasses UI | The app part of My Smart Glasses that can be launched by the user, provides the My Smart Glasses UI, and can be closed by the user while the service is still running. 
toozer | An android application that is installed on the mobile and managed by the user, and can communicate with My Smart Glasses to provide content to and receive input from the tooz DevKit.
toozie | A software extension installed on the My Smart Glasses that uses the I/O interface of My Smart Glasses to provide functionalities on the tooz DevKit (also called miniApp).
