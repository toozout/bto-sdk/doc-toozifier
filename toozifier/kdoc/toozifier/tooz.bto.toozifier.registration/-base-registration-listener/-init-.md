[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [BaseRegistrationListener](index.md) / [&lt;init&gt;](./-init-.md)

# &lt;init&gt;

`BaseRegistrationListener()`

Empty implementation of [RegistrationListener](../-registration-listener/index.md)

