[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [BaseRegistrationListener](index.md) / [onRegisterFailure](./on-register-failure.md)

# onRegisterFailure

`fun onRegisterFailure(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [RegistrationListener.onRegisterFailure](../-registration-listener/on-register-failure.md)

