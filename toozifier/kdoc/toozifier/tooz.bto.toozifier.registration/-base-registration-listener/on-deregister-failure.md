[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [BaseRegistrationListener](index.md) / [onDeregisterFailure](./on-deregister-failure.md)

# onDeregisterFailure

`fun onDeregisterFailure(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [RegistrationListener.onDeregisterFailure](../-registration-listener/on-deregister-failure.md)

