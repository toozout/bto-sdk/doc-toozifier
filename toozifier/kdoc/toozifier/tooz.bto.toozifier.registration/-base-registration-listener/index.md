[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [BaseRegistrationListener](./index.md)

# BaseRegistrationListener

`class BaseRegistrationListener : `[`RegistrationListener`](../-registration-listener/index.md)

Empty implementation of [RegistrationListener](../-registration-listener/index.md)

### Constructors

| Name | Summary |
|---|---|
| [&lt;init&gt;](-init-.md) | `BaseRegistrationListener()`<br>Empty implementation of [RegistrationListener](../-registration-listener/index.md) |

### Functions

| Name | Summary |
|---|---|
| [onDeregisterFailure](on-deregister-failure.md) | `fun onDeregisterFailure(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [onDeregisterSuccess](on-deregister-success.md) | `fun onDeregisterSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [onRegisterFailure](on-register-failure.md) | `fun onRegisterFailure(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [onRegisterSuccess](on-register-success.md) | `fun onRegisterSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
