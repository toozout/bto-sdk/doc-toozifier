[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [BaseRegistrationListener](index.md) / [onRegisterSuccess](./on-register-success.md)

# onRegisterSuccess

`fun onRegisterSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [RegistrationListener.onRegisterSuccess](../-registration-listener/on-register-success.md)

