[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [BaseRegistrationListener](index.md) / [onDeregisterSuccess](./on-deregister-success.md)

# onDeregisterSuccess

`fun onDeregisterSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [RegistrationListener.onDeregisterSuccess](../-registration-listener/on-deregister-success.md)

