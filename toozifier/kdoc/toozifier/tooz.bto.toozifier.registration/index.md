[toozifier](../index.md) / [tooz.bto.toozifier.registration](./index.md)

## Package tooz.bto.toozifier.registration

### Types

| Name | Summary |
|---|---|
| [BaseRegistrationListener](-base-registration-listener/index.md) | `class BaseRegistrationListener : `[`RegistrationListener`](-registration-listener/index.md)<br>Empty implementation of [RegistrationListener](-registration-listener/index.md) |
| [RegistrationListener](-registration-listener/index.md) | `interface RegistrationListener : `[`ToozifierListener`](../tooz.bto.toozifier/-toozifier-listener.md)<br>Defines callbacks regarding Toozer's registration to tooz Control App |
