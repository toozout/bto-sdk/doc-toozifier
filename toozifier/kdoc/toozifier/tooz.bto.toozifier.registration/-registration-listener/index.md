[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [RegistrationListener](./index.md)

# RegistrationListener

`interface RegistrationListener : `[`ToozifierListener`](../../tooz.bto.toozifier/-toozifier-listener.md)

Defines callbacks regarding Toozer's registration to tooz Control App

### Functions

| Name | Summary |
|---|---|
| [onDeregisterFailure](on-deregister-failure.md) | `abstract fun onDeregisterFailure(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [onDeregisterSuccess](on-deregister-success.md) | `abstract fun onDeregisterSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [onRegisterFailure](on-register-failure.md) | `abstract fun onRegisterFailure(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [onRegisterSuccess](on-register-success.md) | `abstract fun onRegisterSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |

### Inheritors

| Name | Summary |
|---|---|
| [BaseRegistrationListener](../-base-registration-listener/index.md) | `class BaseRegistrationListener : `[`RegistrationListener`](./index.md)<br>Empty implementation of [RegistrationListener](./index.md) |
| [GlassesListener](../../tooz.bto.toozifier/-glasses-listener.md) | `interface GlassesListener : `[`RegistrationListener`](./index.md)`, `[`FrameListener`](../../tooz.bto.toozifier.frame/-frame-listener/index.md)`, `[`GlassesControlListener`](../../tooz.bto.toozifier.control/-glasses-control-listener/index.md)`, `[`GlassesStateListener`](../../tooz.bto.toozifier/-glasses-state-listener/index.md)`, `[`ButtonEventListener`](../../tooz.bto.toozifier.button/-button-event-listener/index.md)<br>Interface definition for all general callbacks regarding registration with tooz control App and the status of the glasses. |
