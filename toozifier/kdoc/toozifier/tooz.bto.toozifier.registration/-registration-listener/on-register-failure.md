[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [RegistrationListener](index.md) / [onRegisterFailure](./on-register-failure.md)

# onRegisterFailure

`abstract fun onRegisterFailure(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)