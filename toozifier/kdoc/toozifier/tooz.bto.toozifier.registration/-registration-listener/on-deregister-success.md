[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [RegistrationListener](index.md) / [onDeregisterSuccess](./on-deregister-success.md)

# onDeregisterSuccess

`abstract fun onDeregisterSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)