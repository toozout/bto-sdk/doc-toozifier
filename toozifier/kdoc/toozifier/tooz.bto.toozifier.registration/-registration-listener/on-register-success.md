[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [RegistrationListener](index.md) / [onRegisterSuccess](./on-register-success.md)

# onRegisterSuccess

`abstract fun onRegisterSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)