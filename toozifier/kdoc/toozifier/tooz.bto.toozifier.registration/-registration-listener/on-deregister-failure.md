[toozifier](../../index.md) / [tooz.bto.toozifier.registration](../index.md) / [RegistrationListener](index.md) / [onDeregisterFailure](./on-deregister-failure.md)

# onDeregisterFailure

`abstract fun onDeregisterFailure(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)