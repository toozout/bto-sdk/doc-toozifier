[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCause](index.md) / [description](./description.md)

# description

`val description: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)