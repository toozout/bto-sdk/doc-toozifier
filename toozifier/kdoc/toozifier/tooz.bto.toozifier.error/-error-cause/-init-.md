[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCause](index.md) / [&lt;init&gt;](./-init-.md)

# &lt;init&gt;

`ErrorCause(code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, description: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)`)`