[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [SERVICE_NOT_AVAILABLE](./-s-e-r-v-i-c-e_-n-o-t_-a-v-a-i-l-a-b-l-e.md)

# SERVICE_NOT_AVAILABLE

`const val SERVICE_NOT_AVAILABLE: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)