[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [UNKNOWN](./-u-n-k-n-o-w-n.md)

# UNKNOWN

`const val UNKNOWN: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)