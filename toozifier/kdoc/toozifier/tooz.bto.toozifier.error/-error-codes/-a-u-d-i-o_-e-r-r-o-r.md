[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [AUDIO_ERROR](./-a-u-d-i-o_-e-r-r-o-r.md)

# AUDIO_ERROR

`const val AUDIO_ERROR: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)