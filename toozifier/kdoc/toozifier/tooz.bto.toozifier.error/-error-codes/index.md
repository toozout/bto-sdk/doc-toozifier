[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](./index.md)

# ErrorCodes

`object ErrorCodes`

### Properties

| Name | Summary |
|---|---|
| [ALREADY_REGISTERED](-a-l-r-e-a-d-y_-r-e-g-i-s-t-e-r-e-d.md) | `const val ALREADY_REGISTERED: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [AUDIO_ERROR](-a-u-d-i-o_-e-r-r-o-r.md) | `const val AUDIO_ERROR: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [CANNOT_CREATE_FRAME](-c-a-n-n-o-t_-c-r-e-a-t-e_-f-r-a-m-e.md) | `const val CANNOT_CREATE_FRAME: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [CANNOT_GAIN_CONTROL](-c-a-n-n-o-t_-g-a-i-n_-c-o-n-t-r-o-l.md) | `const val CANNOT_GAIN_CONTROL: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [CONNECTION_ERROR](-c-o-n-n-e-c-t-i-o-n_-e-r-r-o-r.md) | `const val CONNECTION_ERROR: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [NOT_REGISTERED](-n-o-t_-r-e-g-i-s-t-e-r-e-d.md) | `const val NOT_REGISTERED: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [PROTOCOL_ERROR](-p-r-o-t-o-c-o-l_-e-r-r-o-r.md) | `const val PROTOCOL_ERROR: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [SERVICE_ERROR](-s-e-r-v-i-c-e_-e-r-r-o-r.md) | `const val SERVICE_ERROR: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [SERVICE_NOT_AVAILABLE](-s-e-r-v-i-c-e_-n-o-t_-a-v-a-i-l-a-b-l-e.md) | `const val SERVICE_NOT_AVAILABLE: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [TOOZIFIER_VERSION_OUTDATED](-t-o-o-z-i-f-i-e-r_-v-e-r-s-i-o-n_-o-u-t-d-a-t-e-d.md) | `const val TOOZIFIER_VERSION_OUTDATED: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [UNKNOWN](-u-n-k-n-o-w-n.md) | `const val UNKNOWN: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [UNKNOWN_FEATURE](-u-n-k-n-o-w-n_-f-e-a-t-u-r-e.md) | `const val UNKNOWN_FEATURE: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
