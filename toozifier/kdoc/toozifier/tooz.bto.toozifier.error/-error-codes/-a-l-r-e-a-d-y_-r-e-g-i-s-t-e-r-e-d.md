[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [ALREADY_REGISTERED](./-a-l-r-e-a-d-y_-r-e-g-i-s-t-e-r-e-d.md)

# ALREADY_REGISTERED

`const val ALREADY_REGISTERED: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)