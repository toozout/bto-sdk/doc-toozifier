[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [UNKNOWN_FEATURE](./-u-n-k-n-o-w-n_-f-e-a-t-u-r-e.md)

# UNKNOWN_FEATURE

`const val UNKNOWN_FEATURE: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)