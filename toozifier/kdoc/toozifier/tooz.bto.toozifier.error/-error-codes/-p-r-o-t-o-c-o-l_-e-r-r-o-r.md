[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [PROTOCOL_ERROR](./-p-r-o-t-o-c-o-l_-e-r-r-o-r.md)

# PROTOCOL_ERROR

`const val PROTOCOL_ERROR: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)