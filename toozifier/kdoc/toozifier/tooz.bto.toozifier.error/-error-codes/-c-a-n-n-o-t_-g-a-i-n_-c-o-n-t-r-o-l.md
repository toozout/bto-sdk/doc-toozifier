[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [CANNOT_GAIN_CONTROL](./-c-a-n-n-o-t_-g-a-i-n_-c-o-n-t-r-o-l.md)

# CANNOT_GAIN_CONTROL

`const val CANNOT_GAIN_CONTROL: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)