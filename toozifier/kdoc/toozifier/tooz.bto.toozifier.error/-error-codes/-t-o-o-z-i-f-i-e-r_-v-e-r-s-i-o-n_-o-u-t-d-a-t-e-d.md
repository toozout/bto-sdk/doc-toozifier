[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [TOOZIFIER_VERSION_OUTDATED](./-t-o-o-z-i-f-i-e-r_-v-e-r-s-i-o-n_-o-u-t-d-a-t-e-d.md)

# TOOZIFIER_VERSION_OUTDATED

`const val TOOZIFIER_VERSION_OUTDATED: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)