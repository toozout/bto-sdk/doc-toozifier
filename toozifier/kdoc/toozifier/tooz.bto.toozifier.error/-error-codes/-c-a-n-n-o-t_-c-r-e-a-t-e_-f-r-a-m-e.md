[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [CANNOT_CREATE_FRAME](./-c-a-n-n-o-t_-c-r-e-a-t-e_-f-r-a-m-e.md)

# CANNOT_CREATE_FRAME

`const val CANNOT_CREATE_FRAME: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)