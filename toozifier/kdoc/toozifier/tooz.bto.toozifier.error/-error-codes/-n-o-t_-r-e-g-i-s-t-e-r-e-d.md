[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [NOT_REGISTERED](./-n-o-t_-r-e-g-i-s-t-e-r-e-d.md)

# NOT_REGISTERED

`const val NOT_REGISTERED: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)