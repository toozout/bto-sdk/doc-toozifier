[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [CONNECTION_ERROR](./-c-o-n-n-e-c-t-i-o-n_-e-r-r-o-r.md)

# CONNECTION_ERROR

`const val CONNECTION_ERROR: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)