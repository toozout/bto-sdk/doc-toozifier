[toozifier](../../index.md) / [tooz.bto.toozifier.error](../index.md) / [ErrorCodes](index.md) / [SERVICE_ERROR](./-s-e-r-v-i-c-e_-e-r-r-o-r.md)

# SERVICE_ERROR

`const val SERVICE_ERROR: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)