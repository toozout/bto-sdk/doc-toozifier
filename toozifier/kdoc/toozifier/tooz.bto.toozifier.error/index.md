[toozifier](../index.md) / [tooz.bto.toozifier.error](./index.md)

## Package tooz.bto.toozifier.error

### Types

| Name | Summary |
|---|---|
| [ErrorCause](-error-cause/index.md) | `data class ErrorCause` |
| [ErrorCodes](-error-codes/index.md) | `object ErrorCodes` |
