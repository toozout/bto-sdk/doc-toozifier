[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [CardListener](index.md) / [cardUpdateFailed](./card-update-failed.md)

# cardUpdateFailed

`abstract fun cardUpdateFailed(eventCause: `[`EventCause`](../-event-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)