[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [CardListener](index.md) / [cardRemovedSuccessfully](./card-removed-successfully.md)

# cardRemovedSuccessfully

`abstract fun cardRemovedSuccessfully(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)