[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [CardListener](index.md) / [cardPositionChanged](./card-position-changed.md)

# cardPositionChanged

`abstract fun cardPositionChanged(cardPosition: `[`CardPosition`](../-card-position/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)