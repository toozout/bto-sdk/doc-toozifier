[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [CardListener](index.md) / [cardGoingToFrontSlot](./card-going-to-front-slot.md)

# cardGoingToFrontSlot

`abstract fun cardGoingToFrontSlot(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)