[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [CardListener](./index.md)

# CardListener

`interface CardListener : `[`ToozifierListener`](../-toozifier-listener.md)

### Functions

| Name | Summary |
|---|---|
| [cardGoingToFrontSlot](card-going-to-front-slot.md) | `abstract fun cardGoingToFrontSlot(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [cardPositionChanged](card-position-changed.md) | `abstract fun cardPositionChanged(cardPosition: `[`CardPosition`](../-card-position/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [cardRemovedSuccessfully](card-removed-successfully.md) | `abstract fun cardRemovedSuccessfully(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [cardRemoveFailed](card-remove-failed.md) | `abstract fun cardRemoveFailed(eventCause: `[`EventCause`](../-event-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [cardUpdatedSuccessfully](card-updated-successfully.md) | `abstract fun cardUpdatedSuccessfully(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [cardUpdateFailed](card-update-failed.md) | `abstract fun cardUpdateFailed(eventCause: `[`EventCause`](../-event-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
