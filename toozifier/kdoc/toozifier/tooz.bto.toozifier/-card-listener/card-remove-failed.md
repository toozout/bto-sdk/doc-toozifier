[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [CardListener](index.md) / [cardRemoveFailed](./card-remove-failed.md)

# cardRemoveFailed

`abstract fun cardRemoveFailed(eventCause: `[`EventCause`](../-event-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)