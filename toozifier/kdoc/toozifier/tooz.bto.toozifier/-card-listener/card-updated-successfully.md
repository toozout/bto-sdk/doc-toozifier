[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [CardListener](index.md) / [cardUpdatedSuccessfully](./card-updated-successfully.md)

# cardUpdatedSuccessfully

`abstract fun cardUpdatedSuccessfully(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)