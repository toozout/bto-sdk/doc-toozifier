[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [NotificationListener](./index.md)

# NotificationListener

`interface NotificationListener : `[`ToozifierListener`](../-toozifier-listener.md)

Interface definition for callbacks regarding sending of notifications to tooz control App and the glasses.

### Functions

| Name | Summary |
|---|---|
| [notificationDisplayed](notification-displayed.md) | `abstract fun notificationDisplayed(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when the notification is displayed. |
| [onNotificationError](on-notification-error.md) | `abstract fun onNotificationError(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
