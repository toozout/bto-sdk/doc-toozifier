[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [NotificationListener](index.md) / [notificationDisplayed](./notification-displayed.md)

# notificationDisplayed

`abstract fun notificationDisplayed(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when the notification is displayed.

