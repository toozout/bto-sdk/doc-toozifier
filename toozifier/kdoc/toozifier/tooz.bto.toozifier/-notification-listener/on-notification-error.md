[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [NotificationListener](index.md) / [onNotificationError](./on-notification-error.md)

# onNotificationError

`abstract fun onNotificationError(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)