[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [GlassesState](index.md) / [CONTROLLED](./-c-o-n-t-r-o-l-l-e-d.md)

# CONTROLLED

`CONTROLLED`

### Inherited Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
