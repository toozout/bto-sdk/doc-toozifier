[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [GlassesState](index.md) / [fromCode](./from-code.md)

# fromCode

`fun fromCode(code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`GlassesState`](index.md)