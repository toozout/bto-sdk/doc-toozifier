[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [GlassesState](index.md) / [TEMPORARY_CONTROLLED](./-t-e-m-p-o-r-a-r-y_-c-o-n-t-r-o-l-l-e-d.md)

# TEMPORARY_CONTROLLED

`TEMPORARY_CONTROLLED`

### Inherited Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
