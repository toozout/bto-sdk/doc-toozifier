[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [GlassesState](index.md) / [code](./code.md)

# code

`val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)