[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [GlassesState](index.md) / [UNKNOWN](./-u-n-k-n-o-w-n.md)

# UNKNOWN

`UNKNOWN`

### Inherited Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
