[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [GlassesState](./index.md)

# GlassesState

`enum class GlassesState`

Different states of the glasses to be reported by the glasses firmware.

### Enum Values

| Name | Summary |
|---|---|
| [UNKNOWN](-u-n-k-n-o-w-n.md) |  |
| [OFFLINE](-o-f-f-l-i-n-e.md) |  |
| [ONLINE](-o-n-l-i-n-e.md) |  |
| [TEMPORARY_CONTROLLED](-t-e-m-p-o-r-a-r-y_-c-o-n-t-r-o-l-l-e-d.md) |  |
| [CONTROLLED](-c-o-n-t-r-o-l-l-e-d.md) |  |

### Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

### Companion Object Functions

| Name | Summary |
|---|---|
| [fromCode](from-code.md) | `fun fromCode(code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`GlassesState`](./index.md) |
