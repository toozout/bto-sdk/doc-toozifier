[toozifier](../index.md) / [tooz.bto.toozifier](./index.md)

## Package tooz.bto.toozifier

### Types

| Name | Summary |
|---|---|
| [CardListener](-card-listener/index.md) | `interface CardListener : `[`ToozifierListener`](-toozifier-listener.md) |
| [CardPosition](-card-position/index.md) | `enum class CardPosition` |
| [EventCause](-event-cause/index.md) | `enum class EventCause`<br>The cause behind a (mostly unexpected) event from tooz control App |
| [GlassesListener](-glasses-listener.md) | `interface GlassesListener : `[`RegistrationListener`](../tooz.bto.toozifier.registration/-registration-listener/index.md)`, `[`FrameListener`](../tooz.bto.toozifier.frame/-frame-listener/index.md)`, `[`GlassesControlListener`](../tooz.bto.toozifier.control/-glasses-control-listener/index.md)`, `[`GlassesStateListener`](-glasses-state-listener/index.md)`, `[`ButtonEventListener`](../tooz.bto.toozifier.button/-button-event-listener/index.md)<br>Interface definition for all general callbacks regarding registration with tooz control App and the status of the glasses. |
| [GlassesState](-glasses-state/index.md) | `enum class GlassesState`<br>Different states of the glasses to be reported by the glasses firmware. |
| [GlassesStateListener](-glasses-state-listener/index.md) | `interface GlassesStateListener : `[`ToozifierListener`](-toozifier-listener.md) |
| [NotificationListener](-notification-listener/index.md) | `interface NotificationListener : `[`ToozifierListener`](-toozifier-listener.md)<br>Interface definition for callbacks regarding sending of notifications to tooz control App and the glasses. |
| [ToozerState](-toozer-state/index.md) | `interface ToozerState`<br>The base interface of a Toozer state defined by the SDK. |
| [Toozifier](-toozifier/index.md) | `interface Toozifier`<br>Main interface that enables 3rd party apps communication to the tooz glasses through the tooz control App app. |
| [ToozifierFactory](-toozifier-factory/index.md) | `class ToozifierFactory`<br>This class is responsible for generating and managing instances of Toozifier objects |
| [ToozifierListener](-toozifier-listener.md) | `interface ToozifierListener`<br>Base interface of all Toozifier interfaces |
