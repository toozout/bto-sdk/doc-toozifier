[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [ToozifierFactory](./index.md)

# ToozifierFactory

`class ToozifierFactory`

This class is responsible for generating and managing instances of Toozifier objects

### Constructors

| Name | Summary |
|---|---|
| [&lt;init&gt;](-init-.md) | `ToozifierFactory()`<br>This class is responsible for generating and managing instances of Toozifier objects |

### Companion Object Functions

| Name | Summary |
|---|---|
| [getInstance](get-instance.md) | `fun getInstance(): `[`Toozifier`](../-toozifier/index.md) |
