[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [ToozifierFactory](index.md) / [getInstance](./get-instance.md)

# getInstance

`@JvmStatic fun getInstance(): `[`Toozifier`](../-toozifier/index.md)