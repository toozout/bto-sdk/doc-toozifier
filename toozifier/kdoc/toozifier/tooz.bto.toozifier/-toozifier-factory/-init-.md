[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [ToozifierFactory](index.md) / [&lt;init&gt;](./-init-.md)

# &lt;init&gt;

`ToozifierFactory()`

This class is responsible for generating and managing instances of Toozifier objects

