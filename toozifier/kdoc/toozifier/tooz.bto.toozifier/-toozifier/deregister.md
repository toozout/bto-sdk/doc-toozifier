[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [deregister](./deregister.md)

# deregister

`abstract fun deregister(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Deregisters the client from the tooz control App service.

All the listeners will be removed from the tooz control App.

