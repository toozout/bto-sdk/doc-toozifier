[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [isSubscribedToAudio](./is-subscribed-to-audio.md)

# isSubscribedToAudio

`abstract val isSubscribedToAudio: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

**Return**
true if an app is subscribed to audio

