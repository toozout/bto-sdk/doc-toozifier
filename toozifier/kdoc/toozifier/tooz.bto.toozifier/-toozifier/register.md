[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [register](./register.md)

# register

`abstract fun register(context: `[`Context`](https://developer.android.com/reference/android/content/Context.html)`, appName: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)`, registrationListener: `[`RegistrationListener`](../../tooz.bto.toozifier.registration/-registration-listener/index.md)`, cardColor: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

Registers the client with the tooz control App service. If successful, the client
will be able to receive all events related to the glasses.

### Parameters

`context` - context of the client

`appName` - name of the Toozer application

`registrationListener` - listener for all registration events

`cardColor` - color of the card displayed in tooz Control App App

**Return**
True if successfully bond to tooz Control App service

`abstract fun register(context: `[`Context`](https://developer.android.com/reference/android/content/Context.html)`, appName: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)`, registrationListener: `[`RegistrationListener`](../../tooz.bto.toozifier.registration/-registration-listener/index.md)`): `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)