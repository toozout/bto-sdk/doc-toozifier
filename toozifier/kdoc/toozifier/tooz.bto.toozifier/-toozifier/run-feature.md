[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [runFeature](./run-feature.md)

# runFeature

`abstract fun runFeature(uri: `[`URI`](https://developer.android.com/reference/java/net/URI.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Runs a feature specified with an uri

### Parameters

`uri` - URI in format tooz://feature/path