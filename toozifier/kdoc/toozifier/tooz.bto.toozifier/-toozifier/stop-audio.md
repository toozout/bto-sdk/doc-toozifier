[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [stopAudio](./stop-audio.md)

# stopAudio

`abstract fun stopAudio(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Stops the flow of audio data to this client, if the audio has been
successfully requested before.

