[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [startAudio](./start-audio.md)

# startAudio

`abstract fun startAudio(maxDuration: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)` = 10): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Requests tooz control App to send audio packets to this
client. This call can only be successful if the client is already in
control of the glasses.

### Parameters

`maxDuration` - Max duration of audio recording in seconds. Default value is 10.