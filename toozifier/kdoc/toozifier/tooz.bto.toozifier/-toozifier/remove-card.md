[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [removeCard](./remove-card.md)

# removeCard

`abstract fun removeCard(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Removes the card of this app from the stack.

