[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [requestPrompt](./request-prompt.md)

# requestPrompt

`abstract fun requestPrompt(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Request to bring the card to the front slot.

