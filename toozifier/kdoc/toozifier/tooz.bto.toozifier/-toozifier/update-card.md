[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [updateCard](./update-card.md)

# updateCard

`abstract fun updateCard(promptView: `[`View`](https://developer.android.com/reference/android/view/View.html)`, focusView: `[`View`](https://developer.android.com/reference/android/view/View.html)`, timeToLive: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)` = Constants.FRAME_TIME_TO_LIVE_FOREVER): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Extracts a snapshot frame from the two views and uses them to update the card of this app. The
focus view will be valid for a maximum of [timeToLive](update-card.md#tooz.bto.toozifier.Toozifier$updateCard(android.view.View, android.view.View, kotlin.Long)/timeToLive) milliSeconds.
If successful, the client will be granted control of the glasses.

### Parameters

`promptView` - the [View](https://developer.android.com/reference/android/view/View.html) which will be used to update the prompt face of the card

`focusView` - the [View](https://developer.android.com/reference/android/view/View.html) which will be used to update the focus face of the card

`timeToLive` - maximum duration (in milliseconds) for which the focus view is valid.
Maximum duration is defined by
[Constants.FRAME_TIME_TO_LIVE_FOREVER](#)`abstract fun updateCard(promptView: `[`Bitmap`](https://developer.android.com/reference/android/graphics/Bitmap.html)`, focusView: `[`Bitmap`](https://developer.android.com/reference/android/graphics/Bitmap.html)`, timeToLive: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)` = Constants.FRAME_TIME_TO_LIVE_FOREVER): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Uses the two views to update the card of this app. The
focus view will be valid for a maximum of [timeToLive](update-card.md#tooz.bto.toozifier.Toozifier$updateCard(android.graphics.Bitmap, android.graphics.Bitmap, kotlin.Long)/timeToLive) milliSeconds.
If successful, the client will be granted control of the glasses.



### Parameters

`promptView` - the [Bitmap](https://developer.android.com/reference/android/graphics/Bitmap.html) which will be used to update the prompt face of the card

`focusView` - the [Bitmap](https://developer.android.com/reference/android/graphics/Bitmap.html) which will be used to update the focus face of the card

`timeToLive` - maximum duration (in milliseconds) for which the focus view is valid.
Maximum duration is defined by
[Constants.FRAME_TIME_TO_LIVE_FOREVER](#)

### Further explanation

The promptView is shown when this application is displayed as a card, for example when the user switches through the existing apps on the glasses. The focusView is shown when this application is in fullscreen mode on the glasses. Both views can be the same. 

Important to note is that promptView and focusView are objects that will be interacted with, so their properties like width, height and background might be changed by the toozifier library. This means that a view that is already used to be displayed on the screen of an app can not be passed to this method. Instead, a view must be inflated *apart from any existing view hierarchy or screen output* for it to be passed to updateCard. One example would be 
 
    val focusView = (LayoutInflater.from(requireContext()).inflate(R.layout.focus_view, null)
    val promptView = (LayoutInflater.from(requireContext()).inflate(R.layout.prompt_view, null)     	 
    toozifier.updateCard(promptView, focusView, Constants.FRAME_TIME_TO_LIVE_FOREVER)

For the sizing of these views, px should be used instead of dp since the display in the glasses has a fixed size and resolution. The maxium available screen size for the focusView is **400x640** px but is recommended to use the **safe zone of 390x528 px**. The available size for the promptView is **200x320 px**.