[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [removeListener](./remove-listener.md)

# removeListener

`abstract fun removeListener(listener: `[`ToozifierListener`](../-toozifier-listener.md)`): `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

Removes a listener from this toozifier.

### Parameters

`listener` - listener to be removed

**Return**
true if the listener is removed

