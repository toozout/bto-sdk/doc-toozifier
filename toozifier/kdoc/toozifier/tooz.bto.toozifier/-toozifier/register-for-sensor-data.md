[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [registerForSensorData](./register-for-sensor-data.md)

# registerForSensorData

`abstract fun registerForSensorData(vararg sensors: `[`Pair`](https://developer.android.com/reference/android/util/Pair.html)`<`[`Sensor`](../../tooz.bto.toozifier.sensors/-sensor/index.md)`, `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`>): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Requests tooz control App to register for receiving sensor data from the glasses.

### Parameters

`sensors` - one or more pairs of [Constants.Sensor](#) and an
acceptable delay in milliseconds