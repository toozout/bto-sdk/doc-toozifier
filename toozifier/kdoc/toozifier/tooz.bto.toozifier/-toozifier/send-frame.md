[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [sendFrame](./send-frame.md)

# sendFrame

`abstract fun sendFrame(view: `[`View`](https://developer.android.com/reference/android/view/View.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Extracts a snapshot frame from the [view](send-frame.md#tooz.bto.toozifier.Toozifier$sendFrame(android.view.View)/view) and sends it to glasses. The
image will be displayed for a maximum of [timeToLive](#) milliSeconds.
After that it will disappear. If successful, the client will be granted
control of the glasses.

### Parameters

`view` - the View which will be used to extract images from to send`abstract fun sendFrame(view: `[`View`](https://developer.android.com/reference/android/view/View.html)`, timeToLive: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)` = Constants.FRAME_TIME_TO_LIVE_FOREVER): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Extracts a snapshot frame from the [view](send-frame.md#tooz.bto.toozifier.Toozifier$sendFrame(android.view.View, kotlin.Long)/view) and sends it to glasses. The
image will be displayed for a maximum of [timeToLive](send-frame.md#tooz.bto.toozifier.Toozifier$sendFrame(android.view.View, kotlin.Long)/timeToLive) milliSeconds.
After that it will disappear. If successful, the client will be granted
control of the glasses.

### Parameters

`view` - the View which will be used to extract images from to send

`timeToLive` - maximum duration (in milliseconds) for which the frame
can be shown on the glasses. Maximum duration is defined by
[Constants.FRAME_TIME_TO_LIVE_FOREVER](#)`abstract fun sendFrame(bitmap: `[`Bitmap`](https://developer.android.com/reference/android/graphics/Bitmap.html)`, timeToLive: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)` = Constants.FRAME_TIME_TO_LIVE_FOREVER): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Sends the given bitmap to glasses to be displayed for a maximum of
[timeToLive](send-frame.md#tooz.bto.toozifier.Toozifier$sendFrame(android.graphics.Bitmap, kotlin.Long)/timeToLive) milliseconds. After that it will disappear. If successful,
the client will be granted control of the glasses.

### Parameters

`bitmap` - the bitmap to be displayed on screen

`timeToLive` - maximum duration (in milliseconds) for which the frame
can be shown on the glasses. Maximum duration is defined by
[Constants.FRAME_TIME_TO_LIVE_FOREVER](#)