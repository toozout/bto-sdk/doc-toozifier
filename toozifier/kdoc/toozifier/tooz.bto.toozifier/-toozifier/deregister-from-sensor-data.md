[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [deregisterFromSensorData](./deregister-from-sensor-data.md)

# deregisterFromSensorData

`abstract fun deregisterFromSensorData(vararg sensors: `[`Sensor`](../../tooz.bto.toozifier.sensors/-sensor/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Requests tooz control App to deregister from receiving sensor data.

### Parameters

`sensors` - is one or more of [Constants.Sensor](#)