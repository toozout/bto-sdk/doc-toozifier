[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [state](./state.md)

# state

`abstract val state: `[`ToozerState`](../-toozer-state/index.md)

**Return**
state maintained by this toozifier

