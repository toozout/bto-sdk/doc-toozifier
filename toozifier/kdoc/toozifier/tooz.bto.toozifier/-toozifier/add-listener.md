[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [Toozifier](index.md) / [addListener](./add-listener.md)

# addListener

`abstract fun addListener(listener: `[`ToozifierListener`](../-toozifier-listener.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Adds a new listener to this toozifier.

### Parameters

`listener` - listener for Toozifier events