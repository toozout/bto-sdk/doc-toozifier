[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [CardPosition](./index.md)

# CardPosition

`enum class CardPosition`

### Enum Values

| Name | Summary |
|---|---|
| [STACK](-s-t-a-c-k.md) |  |
| [CENTER](-c-e-n-t-e-r.md) |  |
| [FRONT](-f-r-o-n-t.md) |  |
