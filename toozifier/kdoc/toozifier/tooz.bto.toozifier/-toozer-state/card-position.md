[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [ToozerState](index.md) / [cardPosition](./card-position.md)

# cardPosition

`abstract var cardPosition: `[`CardPosition`](../-card-position/index.md)

Returns current position of card in stack

**Return**
card position

