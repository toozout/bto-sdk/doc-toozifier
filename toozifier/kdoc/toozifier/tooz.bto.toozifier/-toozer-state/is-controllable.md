[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [ToozerState](index.md) / [isControllable](./is-controllable.md)

# isControllable

`abstract val isControllable: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

Indicates if the glasses can be controlled at this moment.

**Return**
true if the glasses can be in control

