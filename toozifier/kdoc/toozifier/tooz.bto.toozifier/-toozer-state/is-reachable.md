[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [ToozerState](index.md) / [isReachable](./is-reachable.md)

# isReachable

`abstract val isReachable: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

Returns true if the glasses are connected to tooz control App.

**Return**
true if the glasses are known to be connected to tooz control App; false, otherwise.

