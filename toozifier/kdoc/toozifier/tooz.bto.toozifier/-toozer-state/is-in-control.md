[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [ToozerState](index.md) / [isInControl](./is-in-control.md)

# isInControl

`abstract val isInControl: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

Checks if the current client is in control of the glasses.

**Return**
true if the client is in control, even if the control is temporarily interrupted

