[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [ToozerState](index.md) / [isControlPaused](./is-control-paused.md)

# isControlPaused

`abstract val isControlPaused: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

Checks if the control of this client is paused.

**Return**
true if the control is paused

