[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [ToozerState](./index.md)

# ToozerState

`interface ToozerState`

The base interface of a Toozer state defined by the SDK.

### Properties

| Name | Summary |
|---|---|
| [cardPosition](card-position.md) | `abstract var cardPosition: `[`CardPosition`](../-card-position/index.md)<br>Returns current position of card in stack |
| [isConnected](is-connected.md) | `abstract val isConnected: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)<br>Returns true if this client is connected to tooz control App. This does NOT imply that the glasses are also connected. |
| [isControllable](is-controllable.md) | `abstract val isControllable: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)<br>Indicates if the glasses can be controlled at this moment. |
| [isControlPaused](is-control-paused.md) | `abstract val isControlPaused: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)<br>Checks if the control of this client is paused. |
| [isInControl](is-in-control.md) | `abstract val isInControl: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)<br>Checks if the current client is in control of the glasses. |
| [isReachable](is-reachable.md) | `abstract val isReachable: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)<br>Returns true if the glasses are connected to tooz control App. |
