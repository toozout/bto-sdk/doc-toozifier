[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [ToozerState](index.md) / [isConnected](./is-connected.md)

# isConnected

`abstract val isConnected: `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

Returns true if this client is connected to tooz control App.
This does NOT imply that the glasses are also connected.

**Return**
true if this client is connected to tooz control App

