[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [EventCause](index.md) / [toString](./to-string.md)

# toString

`fun toString(): `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)