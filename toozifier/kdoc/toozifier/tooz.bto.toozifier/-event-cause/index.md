[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [EventCause](./index.md)

# EventCause

`enum class EventCause`

The cause behind a (mostly unexpected) event from tooz control App

### Enum Values

| Name | Summary |
|---|---|
| [UNKNOWN](-u-n-k-n-o-w-n.md) |  |
| [FRAME_DENIED_BUFFER_FULL](-f-r-a-m-e_-d-e-n-i-e-d_-b-u-f-f-e-r_-f-u-l-l.md) |  |
| [FRAME_DENIED_VIEW_CANT_BE_CONVERTED_TO_FRAME](-f-r-a-m-e_-d-e-n-i-e-d_-v-i-e-w_-c-a-n-t_-b-e_-c-o-n-v-e-r-t-e-d_-t-o_-f-r-a-m-e.md) |  |
| [SERVICE_UNAVAILABLE](-s-e-r-v-i-c-e_-u-n-a-v-a-i-l-a-b-l-e.md) |  |
| [SERVICE_NOT_INSTALLED](-s-e-r-v-i-c-e_-n-o-t_-i-n-s-t-a-l-l-e-d.md) |  |
| [GLASSES_UNAVAILABLE](-g-l-a-s-s-e-s_-u-n-a-v-a-i-l-a-b-l-e.md) |  |
| [GLASSES_IN_CONTROL](-g-l-a-s-s-e-s_-i-n_-c-o-n-t-r-o-l.md) |  |
| [ALREADY_REGISTERED](-a-l-r-e-a-d-y_-r-e-g-i-s-t-e-r-e-d.md) |  |
| [NOT_REGISTERED](-n-o-t_-r-e-g-i-s-t-e-r-e-d.md) |  |

### Properties

| Name | Summary |
|---|---|
| [description](description.md) | `val description: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [value](value.md) | `val value: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

### Functions

| Name | Summary |
|---|---|
| [toString](to-string.md) | `fun toString(): `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
