[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [EventCause](index.md) / [SERVICE_NOT_INSTALLED](./-s-e-r-v-i-c-e_-n-o-t_-i-n-s-t-a-l-l-e-d.md)

# SERVICE_NOT_INSTALLED

`SERVICE_NOT_INSTALLED`

### Inherited Properties

| Name | Summary |
|---|---|
| [description](description.md) | `val description: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [value](value.md) | `val value: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

### Inherited Functions

| Name | Summary |
|---|---|
| [toString](to-string.md) | `fun toString(): `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
