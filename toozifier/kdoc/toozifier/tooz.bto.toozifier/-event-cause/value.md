[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [EventCause](index.md) / [value](./value.md)

# value

`val value: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)