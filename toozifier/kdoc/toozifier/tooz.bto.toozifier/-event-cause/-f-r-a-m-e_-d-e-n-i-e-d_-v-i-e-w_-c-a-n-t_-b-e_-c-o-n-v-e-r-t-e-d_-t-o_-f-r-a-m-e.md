[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [EventCause](index.md) / [FRAME_DENIED_VIEW_CANT_BE_CONVERTED_TO_FRAME](./-f-r-a-m-e_-d-e-n-i-e-d_-v-i-e-w_-c-a-n-t_-b-e_-c-o-n-v-e-r-t-e-d_-t-o_-f-r-a-m-e.md)

# FRAME_DENIED_VIEW_CANT_BE_CONVERTED_TO_FRAME

`FRAME_DENIED_VIEW_CANT_BE_CONVERTED_TO_FRAME`

### Inherited Properties

| Name | Summary |
|---|---|
| [description](description.md) | `val description: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [value](value.md) | `val value: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

### Inherited Functions

| Name | Summary |
|---|---|
| [toString](to-string.md) | `fun toString(): `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
