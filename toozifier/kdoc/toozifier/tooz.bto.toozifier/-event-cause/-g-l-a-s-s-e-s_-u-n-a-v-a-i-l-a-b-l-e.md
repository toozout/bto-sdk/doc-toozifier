[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [EventCause](index.md) / [GLASSES_UNAVAILABLE](./-g-l-a-s-s-e-s_-u-n-a-v-a-i-l-a-b-l-e.md)

# GLASSES_UNAVAILABLE

`GLASSES_UNAVAILABLE`

### Inherited Properties

| Name | Summary |
|---|---|
| [description](description.md) | `val description: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [value](value.md) | `val value: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

### Inherited Functions

| Name | Summary |
|---|---|
| [toString](to-string.md) | `fun toString(): `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
