[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [EventCause](index.md) / [FRAME_DENIED_BUFFER_FULL](./-f-r-a-m-e_-d-e-n-i-e-d_-b-u-f-f-e-r_-f-u-l-l.md)

# FRAME_DENIED_BUFFER_FULL

`FRAME_DENIED_BUFFER_FULL`

### Inherited Properties

| Name | Summary |
|---|---|
| [description](description.md) | `val description: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [value](value.md) | `val value: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

### Inherited Functions

| Name | Summary |
|---|---|
| [toString](to-string.md) | `fun toString(): `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
