[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [GlassesStateListener](index.md) / [onGlassesStateChange](./on-glasses-state-change.md)

# onGlassesStateChange

`abstract fun onGlassesStateChange(glassesState: `[`GlassesState`](../-glasses-state/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

The state of glasses has changed

### Parameters

`glassesState` - the new state of the glasses