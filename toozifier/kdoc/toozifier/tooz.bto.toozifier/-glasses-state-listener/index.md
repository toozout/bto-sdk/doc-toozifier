[toozifier](../../index.md) / [tooz.bto.toozifier](../index.md) / [GlassesStateListener](./index.md)

# GlassesStateListener

`interface GlassesStateListener : `[`ToozifierListener`](../-toozifier-listener.md)

### Functions

| Name | Summary |
|---|---|
| [onGlassesStateChange](on-glasses-state-change.md) | `abstract fun onGlassesStateChange(glassesState: `[`GlassesState`](../-glasses-state/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>The state of glasses has changed |

### Inheritors

| Name | Summary |
|---|---|
| [GlassesListener](../-glasses-listener.md) | `interface GlassesListener : `[`RegistrationListener`](../../tooz.bto.toozifier.registration/-registration-listener/index.md)`, `[`FrameListener`](../../tooz.bto.toozifier.frame/-frame-listener/index.md)`, `[`GlassesControlListener`](../../tooz.bto.toozifier.control/-glasses-control-listener/index.md)`, `[`GlassesStateListener`](./index.md)`, `[`ButtonEventListener`](../../tooz.bto.toozifier.button/-button-event-listener/index.md)<br>Interface definition for all general callbacks regarding registration with tooz control App and the status of the glasses. |
