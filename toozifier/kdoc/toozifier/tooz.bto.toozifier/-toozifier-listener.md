[toozifier](../index.md) / [tooz.bto.toozifier](index.md) / [ToozifierListener](./-toozifier-listener.md)

# ToozifierListener

`interface ToozifierListener`

Base interface of all Toozifier interfaces

### Inheritors

| Name | Summary |
|---|---|
| [AudioListener](../tooz.bto.toozifier.audio/-audio-listener/index.md) | `interface AudioListener : `[`ToozifierListener`](./-toozifier-listener.md)<br>Listener interface for receiving audio events from BTO Glasses |
| [ButtonEventListener](../tooz.bto.toozifier.button/-button-event-listener/index.md) | `interface ButtonEventListener : `[`ToozifierListener`](./-toozifier-listener.md) |
| [CardListener](-card-listener/index.md) | `interface CardListener : `[`ToozifierListener`](./-toozifier-listener.md) |
| [FeatureRequestListener](../tooz.bto.toozifier.features/-feature-request-listener/index.md) | `interface FeatureRequestListener : `[`ToozifierListener`](./-toozifier-listener.md) |
| [FrameListener](../tooz.bto.toozifier.frame/-frame-listener/index.md) | `interface FrameListener : `[`ToozifierListener`](./-toozifier-listener.md) |
| [GlassesControlListener](../tooz.bto.toozifier.control/-glasses-control-listener/index.md) | `interface GlassesControlListener : `[`ToozifierListener`](./-toozifier-listener.md) |
| [GlassesStateListener](-glasses-state-listener/index.md) | `interface GlassesStateListener : `[`ToozifierListener`](./-toozifier-listener.md) |
| [NotificationListener](-notification-listener/index.md) | `interface NotificationListener : `[`ToozifierListener`](./-toozifier-listener.md)<br>Interface definition for callbacks regarding sending of notifications to tooz control App and the glasses. |
| [RegistrationListener](../tooz.bto.toozifier.registration/-registration-listener/index.md) | `interface RegistrationListener : `[`ToozifierListener`](./-toozifier-listener.md)<br>Defines callbacks regarding Toozer's registration to tooz Control App |
| [SensorDataListener](../tooz.bto.toozifier.sensors/-sensor-data-listener/index.md) | `interface SensorDataListener : `[`ToozifierListener`](./-toozifier-listener.md) |
