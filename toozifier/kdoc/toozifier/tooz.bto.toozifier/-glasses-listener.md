[toozifier](../index.md) / [tooz.bto.toozifier](index.md) / [GlassesListener](./-glasses-listener.md)

# GlassesListener

`interface GlassesListener : `[`RegistrationListener`](../tooz.bto.toozifier.registration/-registration-listener/index.md)`, `[`FrameListener`](../tooz.bto.toozifier.frame/-frame-listener/index.md)`, `[`GlassesControlListener`](../tooz.bto.toozifier.control/-glasses-control-listener/index.md)`, `[`GlassesStateListener`](-glasses-state-listener/index.md)`, `[`ButtonEventListener`](../tooz.bto.toozifier.button/-button-event-listener/index.md)

Interface definition for all general callbacks regarding registration with tooz control App and the
status of the glasses.

### Inherited Functions

| Name | Summary |
|---|---|
| [onButtonEvent](../tooz.bto.toozifier.button/-button-event-listener/on-button-event.md) | `abstract fun onButtonEvent(button: `[`Button`](../tooz.bto.toozifier.button/-button/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Button event from glasses |
| [onControlError](../tooz.bto.toozifier.control/-glasses-control-listener/on-control-error.md) | `abstract fun onControlError(errorCause: `[`ErrorCause`](../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when there is an error in control change of glasses. |
| [onControlGained](../tooz.bto.toozifier.control/-glasses-control-listener/on-control-gained.md) | `abstract fun onControlGained(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when a client gains control of glasses, meaning the client can control glasses display and is able to receive button events. |
| [onControlInterrupted](../tooz.bto.toozifier.control/-glasses-control-listener/on-control-interrupted.md) | `abstract fun onControlInterrupted(duration: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when a client temporarily loses a control of glasses. The control can be regained or lost. |
| [onControlLost](../tooz.bto.toozifier.control/-glasses-control-listener/on-control-lost.md) | `abstract fun onControlLost(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when a client is no longer in control of glasses. |
| [onDeregisterFailure](../tooz.bto.toozifier.registration/-registration-listener/on-deregister-failure.md) | `abstract fun onDeregisterFailure(errorCause: `[`ErrorCause`](../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [onDeregisterSuccess](../tooz.bto.toozifier.registration/-registration-listener/on-deregister-success.md) | `abstract fun onDeregisterSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [onFrameDisplayed](../tooz.bto.toozifier.frame/-frame-listener/on-frame-displayed.md) | `abstract fun onFrameDisplayed(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, delay: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when the frame has been displayed in the glasses |
| [onFrameError](../tooz.bto.toozifier.frame/-frame-listener/on-frame-error.md) | `abstract fun onFrameError(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, errorCause: `[`ErrorCause`](../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when there is an error in sending, receiving, or displaying the frame |
| [onFrameReceived](../tooz.bto.toozifier.frame/-frame-listener/on-frame-received.md) | `abstract fun onFrameReceived(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when glasses have received the frame and put it in queue |
| [onGlassesStateChange](-glasses-state-listener/on-glasses-state-change.md) | `abstract fun onGlassesStateChange(glassesState: `[`GlassesState`](-glasses-state/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>The state of glasses has changed |
| [onRegisterFailure](../tooz.bto.toozifier.registration/-registration-listener/on-register-failure.md) | `abstract fun onRegisterFailure(errorCause: `[`ErrorCause`](../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [onRegisterSuccess](../tooz.bto.toozifier.registration/-registration-listener/on-register-success.md) | `abstract fun onRegisterSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
