[toozifier](../../index.md) / [tooz.bto.toozifier.button](../index.md) / [ButtonEventListener](index.md) / [onButtonEvent](./on-button-event.md)

# onButtonEvent

`abstract fun onButtonEvent(button: `[`Button`](../-button/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Button event from glasses

### Parameters

`button` -

the button event received from the glasses



*Note*: Future implementations will only support [Button.A_1S](../-button/-a_1-s.md)

