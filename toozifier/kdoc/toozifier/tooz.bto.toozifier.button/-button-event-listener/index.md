[toozifier](../../index.md) / [tooz.bto.toozifier.button](../index.md) / [ButtonEventListener](./index.md)

# ButtonEventListener

`interface ButtonEventListener : `[`ToozifierListener`](../../tooz.bto.toozifier/-toozifier-listener.md)

### Functions

| Name | Summary |
|---|---|
| [onButtonEvent](on-button-event.md) | `abstract fun onButtonEvent(button: `[`Button`](../-button/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Button event from glasses |

### Inheritors

| Name | Summary |
|---|---|
| [GlassesListener](../../tooz.bto.toozifier/-glasses-listener.md) | `interface GlassesListener : `[`RegistrationListener`](../../tooz.bto.toozifier.registration/-registration-listener/index.md)`, `[`FrameListener`](../../tooz.bto.toozifier.frame/-frame-listener/index.md)`, `[`GlassesControlListener`](../../tooz.bto.toozifier.control/-glasses-control-listener/index.md)`, `[`GlassesStateListener`](../../tooz.bto.toozifier/-glasses-state-listener/index.md)`, `[`ButtonEventListener`](./index.md)<br>Interface definition for all general callbacks regarding registration with tooz control App and the status of the glasses. |
| [SimpleButtonEventListener](../-simple-button-event-listener/index.md) | `open class SimpleButtonEventListener : `[`ButtonEventListener`](./index.md) |
