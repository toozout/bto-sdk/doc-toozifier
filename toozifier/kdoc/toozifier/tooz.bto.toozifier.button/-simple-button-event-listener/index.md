[toozifier](../../index.md) / [tooz.bto.toozifier.button](../index.md) / [SimpleButtonEventListener](./index.md)

# SimpleButtonEventListener

`open class SimpleButtonEventListener : `[`ButtonEventListener`](../-button-event-listener/index.md)

### Constructors

| Name | Summary |
|---|---|
| [&lt;init&gt;](-init-.md) | `SimpleButtonEventListener()` |

### Functions

| Name | Summary |
|---|---|
| [onButtonEvent](on-button-event.md) | `open fun onButtonEvent(button: `[`Button`](../-button/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Button event from glasses |
