[toozifier](../../index.md) / [tooz.bto.toozifier.button](../index.md) / [SimpleButtonEventListener](index.md) / [onButtonEvent](./on-button-event.md)

# onButtonEvent

`open fun onButtonEvent(button: `[`Button`](../-button/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [ButtonEventListener.onButtonEvent](../-button-event-listener/on-button-event.md)

Button event from glasses

### Parameters

`button` -

the button event received from the glasses



*Note*: Future implementations will only support [Button.A_1S](../-button/-a_1-s.md)

