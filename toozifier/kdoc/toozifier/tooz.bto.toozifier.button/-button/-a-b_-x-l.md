[toozifier](../../index.md) / [tooz.bto.toozifier.button](../index.md) / [Button](index.md) / [AB_XL](./-a-b_-x-l.md)

# AB_XL

`AB_XL`

### Inherited Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
