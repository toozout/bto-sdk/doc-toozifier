[toozifier](../../index.md) / [tooz.bto.toozifier.button](../index.md) / [Button](index.md) / [B_XL](./-b_-x-l.md)

# B_XL

`B_XL`

### Inherited Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
