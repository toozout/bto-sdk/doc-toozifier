[toozifier](../../index.md) / [tooz.bto.toozifier.button](../index.md) / [Button](index.md) / [code](./code.md)

# code

`val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)