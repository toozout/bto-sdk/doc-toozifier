[toozifier](../../index.md) / [tooz.bto.toozifier.button](../index.md) / [Button](index.md) / [B_1S](./-b_1-s.md)

# B_1S

`B_1S`

### Inherited Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
