[toozifier](../../index.md) / [tooz.bto.toozifier.button](../index.md) / [Button](index.md) / [A_XL](./-a_-x-l.md)

# A_XL

`A_XL`

### Inherited Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
