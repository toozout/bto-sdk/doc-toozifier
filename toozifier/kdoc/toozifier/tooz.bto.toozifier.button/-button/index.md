[toozifier](../../index.md) / [tooz.bto.toozifier.button](../index.md) / [Button](./index.md)

# Button

`enum class Button`

Button codes to communicate between tooz BTO glasses
and tooz control App.

### Enum Values

| Name | Summary |
|---|---|
| [UNKNOWN](-u-n-k-n-o-w-n.md) |  |
| [A_1S](-a_1-s.md) |  |
| [A_1L](-a_1-l.md) |  |
| [A_XL](-a_-x-l.md) |  |
| [A_2S](-a_2-s.md) |  |
| [B_1S](-b_1-s.md) |  |
| [B_1L](-b_1-l.md) |  |
| [B_XL](-b_-x-l.md) |  |
| [B_2S](-b_2-s.md) |  |
| [AB_1S](-a-b_1-s.md) |  |
| [AB_1L](-a-b_1-l.md) |  |
| [AB_XL](-a-b_-x-l.md) |  |
| [AB_2S](-a-b_2-s.md) |  |

### Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

### Companion Object Functions

| Name | Summary |
|---|---|
| [fromCode](from-code.md) | `fun fromCode(code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`Button`](./index.md)`?` |
