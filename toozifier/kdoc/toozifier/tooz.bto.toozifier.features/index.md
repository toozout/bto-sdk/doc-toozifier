[toozifier](../index.md) / [tooz.bto.toozifier.features](./index.md)

## Package tooz.bto.toozifier.features

### Types

| Name | Summary |
|---|---|
| [FeatureRequestListener](-feature-request-listener/index.md) | `interface FeatureRequestListener : `[`ToozifierListener`](../tooz.bto.toozifier/-toozifier-listener.md) |
| [FeatureRequestProcessor](-feature-request-processor/index.md) | `class FeatureRequestProcessor : Chain` |
