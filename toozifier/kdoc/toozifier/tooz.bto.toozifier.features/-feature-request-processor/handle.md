[toozifier](../../index.md) / [tooz.bto.toozifier.features](../index.md) / [FeatureRequestProcessor](index.md) / [handle](./handle.md)

# handle

`protected fun handle(commonMessage: ToozServiceMessage): `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)