[toozifier](../../index.md) / [tooz.bto.toozifier.features](../index.md) / [FeatureRequestProcessor](./index.md)

# FeatureRequestProcessor

`class FeatureRequestProcessor : Chain`

### Constructors

| Name | Summary |
|---|---|
| [&lt;init&gt;](-init-.md) | `FeatureRequestProcessor()` |

### Functions

| Name | Summary |
|---|---|
| [addListener](add-listener.md) | `fun addListener(listener: `[`FeatureRequestListener`](../-feature-request-listener/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [handle](handle.md) | `fun handle(commonMessage: ToozServiceMessage): `[`Boolean`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [removeListener](remove-listener.md) | `fun removeListener(listener: `[`FeatureRequestListener`](../-feature-request-listener/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [removeListeners](remove-listeners.md) | `fun removeListeners(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
| [runFeature](run-feature.md) | `fun runFeature(uri: `[`URI`](https://developer.android.com/reference/java/net/URI.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html) |
