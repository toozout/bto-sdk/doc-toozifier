[toozifier](../../index.md) / [tooz.bto.toozifier.features](../index.md) / [FeatureRequestProcessor](index.md) / [removeListener](./remove-listener.md)

# removeListener

`fun removeListener(listener: `[`FeatureRequestListener`](../-feature-request-listener/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)