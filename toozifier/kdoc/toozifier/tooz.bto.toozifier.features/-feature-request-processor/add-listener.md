[toozifier](../../index.md) / [tooz.bto.toozifier.features](../index.md) / [FeatureRequestProcessor](index.md) / [addListener](./add-listener.md)

# addListener

`fun addListener(listener: `[`FeatureRequestListener`](../-feature-request-listener/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)