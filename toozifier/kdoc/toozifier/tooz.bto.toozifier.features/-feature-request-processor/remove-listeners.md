[toozifier](../../index.md) / [tooz.bto.toozifier.features](../index.md) / [FeatureRequestProcessor](index.md) / [removeListeners](./remove-listeners.md)

# removeListeners

`protected fun removeListeners(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)