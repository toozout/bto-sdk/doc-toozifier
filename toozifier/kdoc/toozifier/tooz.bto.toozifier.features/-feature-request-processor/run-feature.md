[toozifier](../../index.md) / [tooz.bto.toozifier.features](../index.md) / [FeatureRequestProcessor](index.md) / [runFeature](./run-feature.md)

# runFeature

`fun runFeature(uri: `[`URI`](https://developer.android.com/reference/java/net/URI.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)