[toozifier](../../index.md) / [tooz.bto.toozifier.features](../index.md) / [FeatureRequestListener](index.md) / [onFeatureRequestSuccess](./on-feature-request-success.md)

# onFeatureRequestSuccess

`abstract fun onFeatureRequestSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when feature request succeeded

