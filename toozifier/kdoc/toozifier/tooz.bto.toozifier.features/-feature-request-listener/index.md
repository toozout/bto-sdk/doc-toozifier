[toozifier](../../index.md) / [tooz.bto.toozifier.features](../index.md) / [FeatureRequestListener](./index.md)

# FeatureRequestListener

`interface FeatureRequestListener : `[`ToozifierListener`](../../tooz.bto.toozifier/-toozifier-listener.md)

### Functions

| Name | Summary |
|---|---|
| [onFeatureRequestFailure](on-feature-request-failure.md) | `abstract fun onFeatureRequestFailure(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when feature request failed |
| [onFeatureRequestSuccess](on-feature-request-success.md) | `abstract fun onFeatureRequestSuccess(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when feature request succeeded |
