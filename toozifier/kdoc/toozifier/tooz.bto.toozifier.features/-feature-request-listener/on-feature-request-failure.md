[toozifier](../../index.md) / [tooz.bto.toozifier.features](../index.md) / [FeatureRequestListener](index.md) / [onFeatureRequestFailure](./on-feature-request-failure.md)

# onFeatureRequestFailure

`abstract fun onFeatureRequestFailure(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when feature request failed

### Parameters

`errorCause` - A description of a failure