[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioFormat](index.md) / [&lt;init&gt;](./-init-.md)

# &lt;init&gt;

`AudioFormat(samplingRate: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, samplingSize: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, encoding: `[`AudioEncoding`](../-audio-encoding/index.md)`, channel: `[`AudioChannel`](../-audio-channel/index.md)`)`

Represents a format of an audio data sent by BTO Glasses

