[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioFormat](index.md) / [encoding](./encoding.md)

# encoding

`val encoding: `[`AudioEncoding`](../-audio-encoding/index.md)

Encoding of an audio data

### Property

`encoding` - Encoding of an audio data