[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioFormat](index.md) / [samplingRate](./sampling-rate.md)

# samplingRate

`val samplingRate: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)

Sampling rate in Hz

### Property

`samplingRate` - Sampling rate in Hz