[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioFormat](index.md) / [samplingSize](./sampling-size.md)

# samplingSize

`val samplingSize: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)

Sampling size in bits: 8 or 16 bits

### Property

`samplingSize` - Sampling size in bits: 8 or 16 bits