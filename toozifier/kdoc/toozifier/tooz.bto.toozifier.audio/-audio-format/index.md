[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioFormat](./index.md)

# AudioFormat

`class AudioFormat`

Represents a format of an audio data sent by BTO Glasses

### Constructors

| Name | Summary |
|---|---|
| [&lt;init&gt;](-init-.md) | `AudioFormat(samplingRate: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, samplingSize: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, encoding: `[`AudioEncoding`](../-audio-encoding/index.md)`, channel: `[`AudioChannel`](../-audio-channel/index.md)`)`<br>Represents a format of an audio data sent by BTO Glasses |

### Properties

| Name | Summary |
|---|---|
| [channel](channel.md) | `val channel: `[`AudioChannel`](../-audio-channel/index.md)<br>Channel type |
| [encoding](encoding.md) | `val encoding: `[`AudioEncoding`](../-audio-encoding/index.md)<br>Encoding of an audio data |
| [samplingRate](sampling-rate.md) | `val samplingRate: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)<br>Sampling rate in Hz |
| [samplingSize](sampling-size.md) | `val samplingSize: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)<br>Sampling size in bits: 8 or 16 bits |
