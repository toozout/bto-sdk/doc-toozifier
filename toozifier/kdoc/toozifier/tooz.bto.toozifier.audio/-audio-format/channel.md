[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioFormat](index.md) / [channel](./channel.md)

# channel

`val channel: `[`AudioChannel`](../-audio-channel/index.md)

Channel type

### Property

`channel` - Channel type