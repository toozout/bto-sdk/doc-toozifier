[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChannel](index.md) / [STEREO](./-s-t-e-r-e-o.md)

# STEREO

`STEREO`

### Inherited Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
