[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChannel](./index.md)

# AudioChannel

`enum class AudioChannel`

### Enum Values

| Name | Summary |
|---|---|
| [MONO](-m-o-n-o.md) |  |
| [STEREO](-s-t-e-r-e-o.md) |  |

### Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

### Companion Object Functions

| Name | Summary |
|---|---|
| [fromCode](from-code.md) | `fun fromCode(code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`AudioChannel`](./index.md) |
