[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChannel](index.md) / [code](./code.md)

# code

`val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)