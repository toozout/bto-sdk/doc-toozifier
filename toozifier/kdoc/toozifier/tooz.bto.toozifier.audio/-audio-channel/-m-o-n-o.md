[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChannel](index.md) / [MONO](./-m-o-n-o.md)

# MONO

`MONO`

### Inherited Properties

| Name | Summary |
|---|---|
| [code](code.md) | `val code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
