[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChannel](index.md) / [fromCode](./from-code.md)

# fromCode

`fun fromCode(code: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`AudioChannel`](index.md)