[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioListener](index.md) / [onAudioRecordingStarted](./on-audio-recording-started.md)

# onAudioRecordingStarted

`abstract fun onAudioRecordingStarted(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when audio recording is started

