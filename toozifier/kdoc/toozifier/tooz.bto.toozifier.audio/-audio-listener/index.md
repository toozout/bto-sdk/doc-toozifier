[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioListener](./index.md)

# AudioListener

`interface AudioListener : `[`ToozifierListener`](../../tooz.bto.toozifier/-toozifier-listener.md)

Listener interface for receiving audio events from BTO Glasses

### Functions

| Name | Summary |
|---|---|
| [onAudioChunkReceived](on-audio-chunk-received.md) | `abstract fun onAudioChunkReceived(audioChunk: `[`AudioChunk`](../-audio-chunk/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when new [audioChunk](on-audio-chunk-received.md#tooz.bto.toozifier.audio.AudioListener$onAudioChunkReceived(tooz.bto.toozifier.audio.AudioChunk)/audioChunk) is received |
| [onAudioError](on-audio-error.md) | `abstract fun onAudioError(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when when audio related error occurs |
| [onAudioRecordingStarted](on-audio-recording-started.md) | `abstract fun onAudioRecordingStarted(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when audio recording is started |
| [onAudioRecordingStopped](on-audio-recording-stopped.md) | `abstract fun onAudioRecordingStopped(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when audioRecording is stopped |
