[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioListener](index.md) / [onAudioRecordingStopped](./on-audio-recording-stopped.md)

# onAudioRecordingStopped

`abstract fun onAudioRecordingStopped(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when audioRecording is stopped

