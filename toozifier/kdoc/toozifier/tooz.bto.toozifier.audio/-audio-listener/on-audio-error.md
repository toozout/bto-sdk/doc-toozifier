[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioListener](index.md) / [onAudioError](./on-audio-error.md)

# onAudioError

`abstract fun onAudioError(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when when audio related error occurs

