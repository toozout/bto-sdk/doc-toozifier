[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioListener](index.md) / [onAudioChunkReceived](./on-audio-chunk-received.md)

# onAudioChunkReceived

`abstract fun onAudioChunkReceived(audioChunk: `[`AudioChunk`](../-audio-chunk/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when new [audioChunk](on-audio-chunk-received.md#tooz.bto.toozifier.audio.AudioListener$onAudioChunkReceived(tooz.bto.toozifier.audio.AudioChunk)/audioChunk) is received

