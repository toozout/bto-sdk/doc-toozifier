[toozifier](../index.md) / [tooz.bto.toozifier.audio](./index.md)

## Package tooz.bto.toozifier.audio

### Types

| Name | Summary |
|---|---|
| [AudioChannel](-audio-channel/index.md) | `enum class AudioChannel` |
| [AudioChunk](-audio-chunk/index.md) | `class AudioChunk`<br>Represents BTO Glasses audio chunk |
| [AudioEncoding](-audio-encoding/index.md) | `enum class AudioEncoding` |
| [AudioFormat](-audio-format/index.md) | `class AudioFormat`<br>Represents a format of an audio data sent by BTO Glasses |
| [AudioListener](-audio-listener/index.md) | `interface AudioListener : `[`ToozifierListener`](../tooz.bto.toozifier/-toozifier-listener.md)<br>Listener interface for receiving audio events from BTO Glasses |
| [ChunkPosition](-chunk-position/index.md) | `enum class ChunkPosition`<br>Represents a position of an [AudioChunk](-audio-chunk/index.md) in an audio session |
