[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioEncoding](index.md) / [PCM_UNSIGNED](./-p-c-m_-u-n-s-i-g-n-e-d.md)

# PCM_UNSIGNED

`PCM_UNSIGNED`

### Inherited Properties

| Name | Summary |
|---|---|
| [value](value.md) | `val value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
