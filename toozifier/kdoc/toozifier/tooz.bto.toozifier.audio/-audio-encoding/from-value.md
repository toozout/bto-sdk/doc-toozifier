[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioEncoding](index.md) / [fromValue](./from-value.md)

# fromValue

`fun fromValue(value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)`): `[`AudioEncoding`](index.md)