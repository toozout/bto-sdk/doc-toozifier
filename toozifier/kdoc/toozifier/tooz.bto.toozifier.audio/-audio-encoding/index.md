[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioEncoding](./index.md)

# AudioEncoding

`enum class AudioEncoding`

### Enum Values

| Name | Summary |
|---|---|
| [PCM_SIGNED](-p-c-m_-s-i-g-n-e-d.md) |  |
| [PCM_UNSIGNED](-p-c-m_-u-n-s-i-g-n-e-d.md) |  |
| [ULAW](-u-l-a-w.md) |  |

### Properties

| Name | Summary |
|---|---|
| [value](value.md) | `val value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |

### Companion Object Functions

| Name | Summary |
|---|---|
| [fromValue](from-value.md) | `fun fromValue(value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)`): `[`AudioEncoding`](./index.md) |
