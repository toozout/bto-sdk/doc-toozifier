[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioEncoding](index.md) / [PCM_SIGNED](./-p-c-m_-s-i-g-n-e-d.md)

# PCM_SIGNED

`PCM_SIGNED`

### Inherited Properties

| Name | Summary |
|---|---|
| [value](value.md) | `val value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
