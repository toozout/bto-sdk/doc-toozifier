[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioEncoding](index.md) / [value](./value.md)

# value

`val value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)