[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChunk](index.md) / [chunkId](./chunk-id.md)

# chunkId

`val chunkId: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)

Chunk ID

### Property

`chunkId` - Chunk ID