[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChunk](index.md) / [position](./position.md)

# position

`val position: `[`ChunkPosition`](../-chunk-position/index.md)

Position of this chunk in the audio session

### Property

`position` - Position of this chunk in the audio session