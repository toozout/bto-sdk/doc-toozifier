[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChunk](index.md) / [audioFormat](./audio-format.md)

# audioFormat

`val audioFormat: `[`AudioFormat`](../-audio-format/index.md)`?`

Describes the format of the audio data

### Property

`audioFormat` - Describes the format of the audio data