[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChunk](index.md) / [data](./data.md)

# data

`val data: `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)

Raw audio data

### Property

`data` - Raw audio data