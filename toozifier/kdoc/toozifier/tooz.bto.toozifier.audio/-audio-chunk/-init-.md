[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChunk](index.md) / [&lt;init&gt;](./-init-.md)

# &lt;init&gt;

`AudioChunk(chunkId: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`, position: `[`ChunkPosition`](../-chunk-position/index.md)`, data: `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)`, audioFormat: `[`AudioFormat`](../-audio-format/index.md)`? = null)`

Represents BTO Glasses audio chunk

