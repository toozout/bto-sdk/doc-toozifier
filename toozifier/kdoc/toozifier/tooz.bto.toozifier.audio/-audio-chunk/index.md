[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [AudioChunk](./index.md)

# AudioChunk

`class AudioChunk`

Represents BTO Glasses audio chunk

### Constructors

| Name | Summary |
|---|---|
| [&lt;init&gt;](-init-.md) | `AudioChunk(chunkId: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`, position: `[`ChunkPosition`](../-chunk-position/index.md)`, data: `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)`, audioFormat: `[`AudioFormat`](../-audio-format/index.md)`? = null)`<br>Represents BTO Glasses audio chunk |

### Properties

| Name | Summary |
|---|---|
| [audioFormat](audio-format.md) | `val audioFormat: `[`AudioFormat`](../-audio-format/index.md)`?`<br>Describes the format of the audio data |
| [chunkId](chunk-id.md) | `val chunkId: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)<br>Chunk ID |
| [data](data.md) | `val data: `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)<br>Raw audio data |
| [position](position.md) | `val position: `[`ChunkPosition`](../-chunk-position/index.md)<br>Position of this chunk in the audio session |
