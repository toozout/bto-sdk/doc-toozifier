[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [ChunkPosition](index.md) / [START](./-s-t-a-r-t.md)

# START

`START`

### Inherited Properties

| Name | Summary |
|---|---|
| [value](value.md) | `val value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
