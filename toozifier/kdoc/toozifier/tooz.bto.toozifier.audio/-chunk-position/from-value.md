[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [ChunkPosition](index.md) / [fromValue](./from-value.md)

# fromValue

`fun fromValue(value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)`): `[`ChunkPosition`](index.md)

Returns a [ChunkPosition](index.md) for a given [value](from-value.md#tooz.bto.toozifier.audio.ChunkPosition.Companion$fromValue(kotlin.String)/value)

