[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [ChunkPosition](index.md) / [END](./-e-n-d.md)

# END

`END`

### Inherited Properties

| Name | Summary |
|---|---|
| [value](value.md) | `val value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
