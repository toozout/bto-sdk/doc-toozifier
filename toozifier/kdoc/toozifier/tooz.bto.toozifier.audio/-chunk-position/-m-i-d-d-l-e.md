[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [ChunkPosition](index.md) / [MIDDLE](./-m-i-d-d-l-e.md)

# MIDDLE

`MIDDLE`

### Inherited Properties

| Name | Summary |
|---|---|
| [value](value.md) | `val value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
