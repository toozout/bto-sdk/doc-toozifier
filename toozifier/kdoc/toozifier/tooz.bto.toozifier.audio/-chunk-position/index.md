[toozifier](../../index.md) / [tooz.bto.toozifier.audio](../index.md) / [ChunkPosition](./index.md)

# ChunkPosition

`enum class ChunkPosition`

Represents a position of an [AudioChunk](../-audio-chunk/index.md) in an audio session

### Enum Values

| Name | Summary |
|---|---|
| [START](-s-t-a-r-t.md) |  |
| [MIDDLE](-m-i-d-d-l-e.md) |  |
| [END](-e-n-d.md) |  |

### Properties

| Name | Summary |
|---|---|
| [value](value.md) | `val value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |

### Companion Object Functions

| Name | Summary |
|---|---|
| [fromValue](from-value.md) | `fun fromValue(value: `[`String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)`): `[`ChunkPosition`](./index.md)<br>Returns a [ChunkPosition](./index.md) for a given [value](from-value.md#tooz.bto.toozifier.audio.ChunkPosition.Companion$fromValue(kotlin.String)/value) |
