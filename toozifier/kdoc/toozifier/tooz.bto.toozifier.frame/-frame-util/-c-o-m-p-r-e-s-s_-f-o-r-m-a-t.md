[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [FrameUtil](index.md) / [COMPRESS_FORMAT](./-c-o-m-p-r-e-s-s_-f-o-r-m-a-t.md)

# COMPRESS_FORMAT

`val COMPRESS_FORMAT: `[`CompressFormat`](https://developer.android.com/reference/android/graphics/Bitmap/CompressFormat.html)