[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [FrameUtil](index.md) / [getFocusFrameBytes](./get-focus-frame-bytes.md)

# getFocusFrameBytes

`fun getFocusFrameBytes(view: `[`View`](https://developer.android.com/reference/android/view/View.html)`): `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)
`fun getFocusFrameBytes(bitmap: `[`Bitmap`](https://developer.android.com/reference/android/graphics/Bitmap.html)`): `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)