[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [FrameUtil](index.md) / [COMPRESS_QUALITY](./-c-o-m-p-r-e-s-s_-q-u-a-l-i-t-y.md)

# COMPRESS_QUALITY

`const val COMPRESS_QUALITY: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)