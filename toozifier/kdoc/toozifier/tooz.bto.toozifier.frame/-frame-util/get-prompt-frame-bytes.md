[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [FrameUtil](index.md) / [getPromptFrameBytes](./get-prompt-frame-bytes.md)

# getPromptFrameBytes

`fun getPromptFrameBytes(view: `[`View`](https://developer.android.com/reference/android/view/View.html)`): `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)
`fun getPromptFrameBytes(bitmap: `[`Bitmap`](https://developer.android.com/reference/android/graphics/Bitmap.html)`): `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)