[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [FrameUtil](./index.md)

# FrameUtil

`object FrameUtil`

### Properties

| Name | Summary |
|---|---|
| [COMPRESS_FORMAT](-c-o-m-p-r-e-s-s_-f-o-r-m-a-t.md) | `val COMPRESS_FORMAT: `[`CompressFormat`](https://developer.android.com/reference/android/graphics/Bitmap/CompressFormat.html) |
| [COMPRESS_QUALITY](-c-o-m-p-r-e-s-s_-q-u-a-l-i-t-y.md) | `const val COMPRESS_QUALITY: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

### Functions

| Name | Summary |
|---|---|
| [getFocusFrameBytes](get-focus-frame-bytes.md) | `fun getFocusFrameBytes(view: `[`View`](https://developer.android.com/reference/android/view/View.html)`): `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)<br>`fun getFocusFrameBytes(bitmap: `[`Bitmap`](https://developer.android.com/reference/android/graphics/Bitmap.html)`): `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html) |
| [getPromptFrameBytes](get-prompt-frame-bytes.md) | `fun getPromptFrameBytes(view: `[`View`](https://developer.android.com/reference/android/view/View.html)`): `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)<br>`fun getPromptFrameBytes(bitmap: `[`Bitmap`](https://developer.android.com/reference/android/graphics/Bitmap.html)`): `[`ByteArray`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html) |
