[toozifier](../index.md) / [tooz.bto.toozifier.frame](./index.md)

## Package tooz.bto.toozifier.frame

### Types

| Name | Summary |
|---|---|
| [BaseFrameListener](-base-frame-listener/index.md) | `class BaseFrameListener : `[`FrameListener`](-frame-listener/index.md) |
| [FrameListener](-frame-listener/index.md) | `interface FrameListener : `[`ToozifierListener`](../tooz.bto.toozifier/-toozifier-listener.md) |
| [FrameUtil](-frame-util/index.md) | `object FrameUtil` |
