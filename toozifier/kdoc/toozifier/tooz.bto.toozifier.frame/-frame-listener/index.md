[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [FrameListener](./index.md)

# FrameListener

`interface FrameListener : `[`ToozifierListener`](../../tooz.bto.toozifier/-toozifier-listener.md)

### Functions

| Name | Summary |
|---|---|
| [onFrameDisplayed](on-frame-displayed.md) | `abstract fun onFrameDisplayed(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, delay: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when the frame has been displayed in the glasses |
| [onFrameError](on-frame-error.md) | `abstract fun onFrameError(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when there is an error in sending, receiving, or displaying the frame |
| [onFrameReceived](on-frame-received.md) | `abstract fun onFrameReceived(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when glasses have received the frame and put it in queue |

### Inheritors

| Name | Summary |
|---|---|
| [BaseFrameListener](../-base-frame-listener/index.md) | `class BaseFrameListener : `[`FrameListener`](./index.md) |
| [GlassesListener](../../tooz.bto.toozifier/-glasses-listener.md) | `interface GlassesListener : `[`RegistrationListener`](../../tooz.bto.toozifier.registration/-registration-listener/index.md)`, `[`FrameListener`](./index.md)`, `[`GlassesControlListener`](../../tooz.bto.toozifier.control/-glasses-control-listener/index.md)`, `[`GlassesStateListener`](../../tooz.bto.toozifier/-glasses-state-listener/index.md)`, `[`ButtonEventListener`](../../tooz.bto.toozifier.button/-button-event-listener/index.md)<br>Interface definition for all general callbacks regarding registration with tooz control App and the status of the glasses. |
