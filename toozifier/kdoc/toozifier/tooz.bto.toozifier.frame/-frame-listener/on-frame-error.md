[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [FrameListener](index.md) / [onFrameError](./on-frame-error.md)

# onFrameError

`abstract fun onFrameError(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when there is an error in sending, receiving, or displaying the
frame

### Parameters

`frameId` - the received frame id

`errorCause` - the cause of the error