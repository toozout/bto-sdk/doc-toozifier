[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [FrameListener](index.md) / [onFrameReceived](./on-frame-received.md)

# onFrameReceived

`abstract fun onFrameReceived(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when glasses have received the frame and put it in queue

### Parameters

`frameId` - the received frame id