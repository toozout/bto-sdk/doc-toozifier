[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [BaseFrameListener](./index.md)

# BaseFrameListener

`class BaseFrameListener : `[`FrameListener`](../-frame-listener/index.md)

### Constructors

| Name | Summary |
|---|---|
| [&lt;init&gt;](-init-.md) | `BaseFrameListener()` |

### Functions

| Name | Summary |
|---|---|
| [onFrameDisplayed](on-frame-displayed.md) | `fun onFrameDisplayed(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, delay: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when the frame has been displayed in the glasses |
| [onFrameError](on-frame-error.md) | `fun onFrameError(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when there is an error in sending, receiving, or displaying the frame |
| [onFrameReceived](on-frame-received.md) | `fun onFrameReceived(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when glasses have received the frame and put it in queue |
