[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [BaseFrameListener](index.md) / [onFrameReceived](./on-frame-received.md)

# onFrameReceived

`fun onFrameReceived(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [FrameListener.onFrameReceived](../-frame-listener/on-frame-received.md)

Called when glasses have received the frame and put it in queue

### Parameters

`frameId` - the received frame id