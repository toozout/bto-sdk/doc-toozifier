[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [BaseFrameListener](index.md) / [onFrameError](./on-frame-error.md)

# onFrameError

`fun onFrameError(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [FrameListener.onFrameError](../-frame-listener/on-frame-error.md)

Called when there is an error in sending, receiving, or displaying the
frame

### Parameters

`frameId` - the received frame id

`errorCause` - the cause of the error