[toozifier](../../index.md) / [tooz.bto.toozifier.frame](../index.md) / [BaseFrameListener](index.md) / [onFrameDisplayed](./on-frame-displayed.md)

# onFrameDisplayed

`fun onFrameDisplayed(frameId: `[`Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)`, delay: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [FrameListener.onFrameDisplayed](../-frame-listener/on-frame-displayed.md)

Called when the frame has been displayed in the glasses

### Parameters

`frameId` - the received frame id

`delay` - in milliseconds, the time between frame received and
frame displayed. Zero means the value is not available.