[toozifier](../../index.md) / [tooz.bto.toozifier.control](../index.md) / [GlassesControlListener](index.md) / [onControlInterrupted](./on-control-interrupted.md)

# onControlInterrupted

`abstract fun onControlInterrupted(duration: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when a client temporarily loses a control of glasses. The control
can be regained or lost.

### Parameters

`duration` - in millis, indicates the maximum length of time until
the control is regained or lost. Zero means the value is unknown.