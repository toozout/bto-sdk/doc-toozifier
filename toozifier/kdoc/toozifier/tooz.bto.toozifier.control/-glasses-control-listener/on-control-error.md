[toozifier](../../index.md) / [tooz.bto.toozifier.control](../index.md) / [GlassesControlListener](index.md) / [onControlError](./on-control-error.md)

# onControlError

`abstract fun onControlError(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when there is an error in control change of glasses.

### Parameters

`errorCause` - the cause of an error