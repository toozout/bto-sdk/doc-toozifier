[toozifier](../../index.md) / [tooz.bto.toozifier.control](../index.md) / [GlassesControlListener](./index.md)

# GlassesControlListener

`interface GlassesControlListener : `[`ToozifierListener`](../../tooz.bto.toozifier/-toozifier-listener.md)

### Functions

| Name | Summary |
|---|---|
| [onControlError](on-control-error.md) | `abstract fun onControlError(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when there is an error in control change of glasses. |
| [onControlGained](on-control-gained.md) | `abstract fun onControlGained(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when a client gains control of glasses, meaning the client can control glasses display and is able to receive button events. |
| [onControlInterrupted](on-control-interrupted.md) | `abstract fun onControlInterrupted(duration: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when a client temporarily loses a control of glasses. The control can be regained or lost. |
| [onControlLost](on-control-lost.md) | `abstract fun onControlLost(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when a client is no longer in control of glasses. |

### Inheritors

| Name | Summary |
|---|---|
| [BaseGlassesControlListener](../-base-glasses-control-listener/index.md) | `class BaseGlassesControlListener : `[`GlassesControlListener`](./index.md) |
| [GlassesListener](../../tooz.bto.toozifier/-glasses-listener.md) | `interface GlassesListener : `[`RegistrationListener`](../../tooz.bto.toozifier.registration/-registration-listener/index.md)`, `[`FrameListener`](../../tooz.bto.toozifier.frame/-frame-listener/index.md)`, `[`GlassesControlListener`](./index.md)`, `[`GlassesStateListener`](../../tooz.bto.toozifier/-glasses-state-listener/index.md)`, `[`ButtonEventListener`](../../tooz.bto.toozifier.button/-button-event-listener/index.md)<br>Interface definition for all general callbacks regarding registration with tooz control App and the status of the glasses. |
