[toozifier](../../index.md) / [tooz.bto.toozifier.control](../index.md) / [GlassesControlListener](index.md) / [onControlGained](./on-control-gained.md)

# onControlGained

`abstract fun onControlGained(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when a client gains control of glasses, meaning the client can
control glasses display and is able to receive button events.

