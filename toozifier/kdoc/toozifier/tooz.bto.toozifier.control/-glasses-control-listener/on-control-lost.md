[toozifier](../../index.md) / [tooz.bto.toozifier.control](../index.md) / [GlassesControlListener](index.md) / [onControlLost](./on-control-lost.md)

# onControlLost

`abstract fun onControlLost(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when a client is no longer in control of glasses.

