[toozifier](../index.md) / [tooz.bto.toozifier.control](./index.md)

## Package tooz.bto.toozifier.control

### Types

| Name | Summary |
|---|---|
| [BaseGlassesControlListener](-base-glasses-control-listener/index.md) | `class BaseGlassesControlListener : `[`GlassesControlListener`](-glasses-control-listener/index.md) |
| [GlassesControlListener](-glasses-control-listener/index.md) | `interface GlassesControlListener : `[`ToozifierListener`](../tooz.bto.toozifier/-toozifier-listener.md) |
