[toozifier](../../index.md) / [tooz.bto.toozifier.control](../index.md) / [BaseGlassesControlListener](index.md) / [onControlError](./on-control-error.md)

# onControlError

`fun onControlError(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [GlassesControlListener.onControlError](../-glasses-control-listener/on-control-error.md)

Called when there is an error in control change of glasses.

### Parameters

`errorCause` - the cause of an error