[toozifier](../../index.md) / [tooz.bto.toozifier.control](../index.md) / [BaseGlassesControlListener](./index.md)

# BaseGlassesControlListener

`class BaseGlassesControlListener : `[`GlassesControlListener`](../-glasses-control-listener/index.md)

### Constructors

| Name | Summary |
|---|---|
| [&lt;init&gt;](-init-.md) | `BaseGlassesControlListener()` |

### Functions

| Name | Summary |
|---|---|
| [onControlError](on-control-error.md) | `fun onControlError(errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when there is an error in control change of glasses. |
| [onControlGained](on-control-gained.md) | `fun onControlGained(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when a client gains control of glasses, meaning the client can control glasses display and is able to receive button events. |
| [onControlInterrupted](on-control-interrupted.md) | `fun onControlInterrupted(duration: `[`Long`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when a client temporarily loses a control of glasses. The control can be regained or lost. |
| [onControlLost](on-control-lost.md) | `fun onControlLost(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when a client is no longer in control of glasses. |
