[toozifier](../../index.md) / [tooz.bto.toozifier.control](../index.md) / [BaseGlassesControlListener](index.md) / [onControlGained](./on-control-gained.md)

# onControlGained

`fun onControlGained(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [GlassesControlListener.onControlGained](../-glasses-control-listener/on-control-gained.md)

Called when a client gains control of glasses, meaning the client can
control glasses display and is able to receive button events.

