[toozifier](../../index.md) / [tooz.bto.toozifier.control](../index.md) / [BaseGlassesControlListener](index.md) / [onControlLost](./on-control-lost.md)

# onControlLost

`fun onControlLost(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [GlassesControlListener.onControlLost](../-glasses-control-listener/on-control-lost.md)

Called when a client is no longer in control of glasses.

