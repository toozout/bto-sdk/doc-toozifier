

### All Types

| Name | Summary |
|---|---|
| [tooz.bto.toozifier.audio.AudioChannel](../tooz.bto.toozifier.audio/-audio-channel/index.md) |  |
| [tooz.bto.toozifier.audio.AudioChunk](../tooz.bto.toozifier.audio/-audio-chunk/index.md) | Represents BTO Glasses audio chunk |
| [tooz.bto.toozifier.audio.AudioEncoding](../tooz.bto.toozifier.audio/-audio-encoding/index.md) |  |
| [tooz.bto.toozifier.audio.AudioFormat](../tooz.bto.toozifier.audio/-audio-format/index.md) | Represents a format of an audio data sent by BTO Glasses |
| [tooz.bto.toozifier.audio.AudioListener](../tooz.bto.toozifier.audio/-audio-listener/index.md) | Listener interface for receiving audio events from BTO Glasses |
| [tooz.bto.toozifier.frame.BaseFrameListener](../tooz.bto.toozifier.frame/-base-frame-listener/index.md) |  |
| [tooz.bto.toozifier.control.BaseGlassesControlListener](../tooz.bto.toozifier.control/-base-glasses-control-listener/index.md) |  |
| [tooz.bto.toozifier.registration.BaseRegistrationListener](../tooz.bto.toozifier.registration/-base-registration-listener/index.md) | Empty implementation of [RegistrationListener](../tooz.bto.toozifier.registration/-registration-listener/index.md) |
| [tooz.bto.toozifier.button.Button](../tooz.bto.toozifier.button/-button/index.md) | Button codes to communicate between tooz BTO glasses and tooz control App. |
| [tooz.bto.toozifier.button.ButtonEventListener](../tooz.bto.toozifier.button/-button-event-listener/index.md) |  |
| [tooz.bto.toozifier.CardListener](../tooz.bto.toozifier/-card-listener/index.md) |  |
| [tooz.bto.toozifier.CardPosition](../tooz.bto.toozifier/-card-position/index.md) |  |
| [tooz.bto.toozifier.audio.ChunkPosition](../tooz.bto.toozifier.audio/-chunk-position/index.md) | Represents a position of an [AudioChunk](../tooz.bto.toozifier.audio/-audio-chunk/index.md) in an audio session |
| [tooz.bto.toozifier.error.ErrorCause](../tooz.bto.toozifier.error/-error-cause/index.md) |  |
| [tooz.bto.toozifier.error.ErrorCodes](../tooz.bto.toozifier.error/-error-codes/index.md) |  |
| [tooz.bto.toozifier.EventCause](../tooz.bto.toozifier/-event-cause/index.md) | The cause behind a (mostly unexpected) event from tooz control App |
| [tooz.bto.toozifier.features.FeatureRequestListener](../tooz.bto.toozifier.features/-feature-request-listener/index.md) |  |
| [tooz.bto.toozifier.features.FeatureRequestProcessor](../tooz.bto.toozifier.features/-feature-request-processor/index.md) |  |
| [tooz.bto.toozifier.frame.FrameListener](../tooz.bto.toozifier.frame/-frame-listener/index.md) |  |
| [tooz.bto.toozifier.frame.FrameUtil](../tooz.bto.toozifier.frame/-frame-util/index.md) |  |
| [tooz.bto.toozifier.control.GlassesControlListener](../tooz.bto.toozifier.control/-glasses-control-listener/index.md) |  |
| [tooz.bto.toozifier.GlassesListener](../tooz.bto.toozifier/-glasses-listener.md) | Interface definition for all general callbacks regarding registration with tooz control App and the status of the glasses. |
| [tooz.bto.toozifier.GlassesState](../tooz.bto.toozifier/-glasses-state/index.md) | Different states of the glasses to be reported by the glasses firmware. |
| [tooz.bto.toozifier.GlassesStateListener](../tooz.bto.toozifier/-glasses-state-listener/index.md) |  |
| [tooz.bto.toozifier.NotificationListener](../tooz.bto.toozifier/-notification-listener/index.md) | Interface definition for callbacks regarding sending of notifications to tooz control App and the glasses. |
| [tooz.bto.toozifier.registration.RegistrationListener](../tooz.bto.toozifier.registration/-registration-listener/index.md) | Defines callbacks regarding Toozer's registration to tooz Control App |
| [tooz.bto.toozifier.sensors.Sensor](../tooz.bto.toozifier.sensors/-sensor/index.md) | Various Sensor reading types. NOTE: Please make sure that this class is mirror representation with class in LibBluetooth library. |
| [tooz.bto.toozifier.sensors.SensorDataListener](../tooz.bto.toozifier.sensors/-sensor-data-listener/index.md) |  |
| [tooz.bto.toozifier.button.SimpleButtonEventListener](../tooz.bto.toozifier.button/-simple-button-event-listener/index.md) |  |
| [tooz.bto.toozifier.sensors.SimpleSensorDataListener](../tooz.bto.toozifier.sensors/-simple-sensor-data-listener/index.md) |  |
| [tooz.bto.toozifier.ToozerState](../tooz.bto.toozifier/-toozer-state/index.md) | The base interface of a Toozer state defined by the SDK. |
| [tooz.bto.toozifier.Toozifier](../tooz.bto.toozifier/-toozifier/index.md) | Main interface that enables 3rd party apps communication to the tooz glasses through the tooz control App app. |
| [tooz.bto.toozifier.ToozifierFactory](../tooz.bto.toozifier/-toozifier-factory/index.md) | This class is responsible for generating and managing instances of Toozifier objects |
| [tooz.bto.toozifier.ToozifierListener](../tooz.bto.toozifier/-toozifier-listener.md) | Base interface of all Toozifier interfaces |
