[toozifier](../index.md) / [tooz.bto.toozifier.sensors](./index.md)

## Package tooz.bto.toozifier.sensors

### Types

| Name | Summary |
|---|---|
| [Sensor](-sensor/index.md) | `enum class Sensor`<br>Various Sensor reading types. NOTE: Please make sure that this class is mirror representation with class in LibBluetooth library. |
| [SensorDataListener](-sensor-data-listener/index.md) | `interface SensorDataListener : `[`ToozifierListener`](../tooz.bto.toozifier/-toozifier-listener.md) |
| [SimpleSensorDataListener](-simple-sensor-data-listener/index.md) | `open class SimpleSensorDataListener : `[`SensorDataListener`](-sensor-data-listener/index.md) |
