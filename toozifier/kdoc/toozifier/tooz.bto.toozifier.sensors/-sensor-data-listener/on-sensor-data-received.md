[toozifier](../../index.md) / [tooz.bto.toozifier.sensors](../index.md) / [SensorDataListener](index.md) / [onSensorDataReceived](./on-sensor-data-received.md)

# onSensorDataReceived

`abstract fun onSensorDataReceived(sensorReading: SensorReading): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Sensor data is ready for consumption

### Parameters

`sensorReading` - the the requested sensors' data