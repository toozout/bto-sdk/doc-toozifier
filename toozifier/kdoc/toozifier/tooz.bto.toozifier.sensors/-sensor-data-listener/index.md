[toozifier](../../index.md) / [tooz.bto.toozifier.sensors](../index.md) / [SensorDataListener](./index.md)

# SensorDataListener

`interface SensorDataListener : `[`ToozifierListener`](../../tooz.bto.toozifier/-toozifier-listener.md)

### Functions

| Name | Summary |
|---|---|
| [onSensorDataDeregistered](on-sensor-data-deregistered.md) | `abstract fun onSensorDataDeregistered(sensor: `[`Sensor`](../-sensor/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Sensor data has been deregistered |
| [onSensorDataReceived](on-sensor-data-received.md) | `abstract fun onSensorDataReceived(sensorReading: SensorReading): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Sensor data is ready for consumption |
| [onSensorDataRegistered](on-sensor-data-registered.md) | `abstract fun onSensorDataRegistered(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>SensorReading has been registered |
| [onSensorError](on-sensor-error.md) | `abstract fun onSensorError(sensor: `[`Sensor`](../-sensor/index.md)`, errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>Called when when audio related error occurs |
| [onSensorListReceived](on-sensor-list-received.md) | `abstract fun onSensorListReceived(sensors: `[`List`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)`<`[`Sensor`](../-sensor/index.md)`>): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)<br>List of sensors that are supported by connected device |

### Inheritors

| Name | Summary |
|---|---|
| [SimpleSensorDataListener](../-simple-sensor-data-listener/index.md) | `open class SimpleSensorDataListener : `[`SensorDataListener`](./index.md) |
