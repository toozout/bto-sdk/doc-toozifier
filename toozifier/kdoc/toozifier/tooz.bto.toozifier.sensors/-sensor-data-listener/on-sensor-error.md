[toozifier](../../index.md) / [tooz.bto.toozifier.sensors](../index.md) / [SensorDataListener](index.md) / [onSensorError](./on-sensor-error.md)

# onSensorError

`abstract fun onSensorError(sensor: `[`Sensor`](../-sensor/index.md)`, errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Called when when audio related error occurs

