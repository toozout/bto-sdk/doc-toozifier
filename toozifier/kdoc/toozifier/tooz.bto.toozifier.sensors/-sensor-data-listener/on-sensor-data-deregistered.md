[toozifier](../../index.md) / [tooz.bto.toozifier.sensors](../index.md) / [SensorDataListener](index.md) / [onSensorDataDeregistered](./on-sensor-data-deregistered.md)

# onSensorDataDeregistered

`abstract fun onSensorDataDeregistered(sensor: `[`Sensor`](../-sensor/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Sensor data has been deregistered

### Parameters

`sensor` - stopped sensor