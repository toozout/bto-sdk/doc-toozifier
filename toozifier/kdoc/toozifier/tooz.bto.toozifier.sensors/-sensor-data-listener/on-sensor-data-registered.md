[toozifier](../../index.md) / [tooz.bto.toozifier.sensors](../index.md) / [SensorDataListener](index.md) / [onSensorDataRegistered](./on-sensor-data-registered.md)

# onSensorDataRegistered

`abstract fun onSensorDataRegistered(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

SensorReading has been registered

