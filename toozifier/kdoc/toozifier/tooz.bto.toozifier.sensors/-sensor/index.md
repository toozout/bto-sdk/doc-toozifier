[toozifier](../../index.md) / [tooz.bto.toozifier.sensors](../index.md) / [Sensor](./index.md)

# Sensor

`enum class Sensor`

Various Sensor reading types.
NOTE: Please make sure that this class is mirror representation
with class in LibBluetooth library.

### Enum Values

| Name | Summary |
|---|---|
| [acceleration](acceleration.md) |  |
| [gameRotation](game-rotation.md) |  |
| [geomagRotation](geomag-rotation.md) |  |
| [gyroscope](gyroscope.md) |  |
| [hinge](hinge.md) |  |
| [light](light.md) |  |
| [magneticField](magnetic-field.md) |  |
| [orientation](orientation.md) |  |
| [pickup](pickup.md) |  |
| [proximity](proximity.md) |  |
| [rawProximity](raw-proximity.md) |  |
| [rotation](rotation.md) |  |
| [step](step.md) |  |
| [stepCounter](step-counter.md) |  |
| [temperature](temperature.md) |  |
| [tilt](tilt.md) |  |
