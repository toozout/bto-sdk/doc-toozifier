[toozifier](../../index.md) / [tooz.bto.toozifier.sensors](../index.md) / [SimpleSensorDataListener](index.md) / [onSensorError](./on-sensor-error.md)

# onSensorError

`open fun onSensorError(sensor: `[`Sensor`](../-sensor/index.md)`, errorCause: `[`ErrorCause`](../../tooz.bto.toozifier.error/-error-cause/index.md)`): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [SensorDataListener.onSensorError](../-sensor-data-listener/on-sensor-error.md)

Called when when audio related error occurs

