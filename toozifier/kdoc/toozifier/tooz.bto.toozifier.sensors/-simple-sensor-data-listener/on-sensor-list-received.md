[toozifier](../../index.md) / [tooz.bto.toozifier.sensors](../index.md) / [SimpleSensorDataListener](index.md) / [onSensorListReceived](./on-sensor-list-received.md)

# onSensorListReceived

`open fun onSensorListReceived(sensors: `[`List`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)`<`[`Sensor`](../-sensor/index.md)`>): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [SensorDataListener.onSensorListReceived](../-sensor-data-listener/on-sensor-list-received.md)

List of sensors that are supported by connected device

### Parameters

`sensors` - supported sensors list