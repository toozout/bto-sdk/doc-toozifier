[toozifier](../../index.md) / [tooz.bto.toozifier.sensors](../index.md) / [SimpleSensorDataListener](index.md) / [onSensorDataReceived](./on-sensor-data-received.md)

# onSensorDataReceived

`open fun onSensorDataReceived(sensorReading: SensorReading): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [SensorDataListener.onSensorDataReceived](../-sensor-data-listener/on-sensor-data-received.md)

Sensor data is ready for consumption

### Parameters

`sensorReading` - the the requested sensors' data