[toozifier](../../index.md) / [tooz.bto.toozifier.sensors](../index.md) / [SimpleSensorDataListener](index.md) / [onSensorDataRegistered](./on-sensor-data-registered.md)

# onSensorDataRegistered

`open fun onSensorDataRegistered(): `[`Unit`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)

Overrides [SensorDataListener.onSensorDataRegistered](../-sensor-data-listener/on-sensor-data-registered.md)

SensorReading has been registered

