# Toozifier Documentation

Toozifier is an Android library that provides API for building Android applications for tooz technologies BTO glasses.  

## Requirements

The library and the tooz control App require a device running Android 7.0 or newer. 

## Using the Library

Follow these steps to use the client library:

### The following steps to add the dependency are deprecated, for now you can simply download the AAR file here: [toozifier SDK](toozifier/artifacts/toozifier-3.0.5.aar)

#### DEPRECATED
### 1. Add Repository

Add the following repository to your maven settings:

```
https://packagecloud.io/priv/a6a37650d06c0577abe2f3945bdb0094163afbc8699345e8/tooz/bto/maven2

```

If you are using Gradle, this would mean add a line ot the `repositories` section of your `build.gradle` file:

```
repositories {
    maven {
        url 'https://packagecloud.io/priv/a6a37650d06c0577abe2f3945bdb0094163afbc8699345e8/tooz/bto/maven2'
    }
}
```

Note: In newer gradle versions, the repository has to be added to the settings.gradle file. Also, the repositoriesMode has to be set to PREFER_SETTINGS.  

Here is an example of how that would look like:  

```
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.PREFER_SETTINGS)
    repositories {
        google()
        mavenCentral()
        maven {
            url 'https://packagecloud.io/priv/a6a37650d06c0577abe2f3945bdb0094163afbc8699345e8/tooz/bto/maven2'
        }
        jcenter() // Warning: this repository is going to shut down soon
    }
}
```

We are aspiring to refactor in the future so that PREFER_SETTINGS does not have to be used.

### 2. Add the Dependency

Add the following dependency to your project using the latest library version:

```
tech.tooz.bto:toozifier:[VERSION]@aar
```

using the latest version of the library.

If you are using Gradle, that would mean adding the following line to the `dependencies` section of your `build.gradle` file:

```
implementation ('tech.tooz.bto:toozifier:[VERSION]@aar') { transitive = true }
```

**Note:** Do not forget to replace `[VERSION]` with the latest version of the library. 

**Version**: The current version of the library is `3.0.5.`.  

You also have to add this query to your Manifest-file:  

```
    <queries>
        <package android:name="com.tooztech.bto.toozos" />
    </queries>
```

## API documentation

Start with API documentation from [toozifier package page](kdoc/toozifier/tooz.bto.toozifier/index.md).

There, you will find core components of the library: [Toozifier](kdoc/toozifier/tooz.bto.toozifier/-toozifier/index.md) and [ToozifierFactory](kdoc/toozifier/tooz.bto.toozifier/-toozifier-factory/index.md).  

**Note about Button-Events: As of now, only the short press on the touchpad can be consumed by client apps!** 

## Tutorials

You can find example usages of this library in the [examples](https://gitlab.com/toozout/bto-sdk/toozer-examples) repository.
