#!/bin/bash
#
# Updates the toozifier documents according to the library docs archive
#

ROOT_DIR=$(git rev-parse --show-toplevel)
DOC_ARCHIVES_FILE="$1"

if [ -z "$DOC_ARCHIVES_FILE" ] || [ ! -e "$DOC_ARCHIVES_FILE" ]
then 
    echo "Error: archives file is missing."
    echo "Usage: $0 <toozifier-doc-zip-file>"
    exit 1
fi

TEMPDIR=$(mktemp -d)
unzip -d $TEMPDIR "$DOC_ARCHIVES_FILE" 
rm -rf `ls $ROOT_DIR | grep -v scripts | grep -v zip$`
cp -r $TEMPDIR/doc/* $ROOT_DIR
