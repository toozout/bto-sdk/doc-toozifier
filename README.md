# tooz Android SDK 

## Introduction

The tooz Android SDK is a set of tools and documents that enable developers to build applications for the tooz DevKit 20.
To start-off, we recommend to take a quick look at the [**Guide Book**](guidebook.md) which shortly describes the main system components. 

## Toozifier Library

Toozifier is an Android library that provides an API for building Android applications for the tooz smart glasses. Check the documents under the [**toozifier**](toozifier/) folder to learn about the core concepts and how to use the library. 

## User Guide

To quickly understand how to use the tooz glasses, feel free to check out the [**User Guide**](setup.md). 

## Downloads & Examples

With the [**these download links**](downloads.md) you can easily download the required applications. Using the tooz Emulator on a second mobile device (as a stand-in for the tooz DevKit 20), you can already test your created applications.  

We also provide some [**demo-applications**](https://gitlab.com/toozout/bto-sdk/toozer-examples) to quickly get started.

## App Design Implications  

Find general app design implications for the tooz glasses in [**Figma**](https://www.figma.com/file/zTSPZLIkCQqys7PwlT8mNt/tooz-App-Design-Implications-and-Examples?node-id=3%3A6559).

## Show your work

Ready to share your application with the outside world? We are happy to support you by hosting your app on our server and by promoting it on our website. Contact us at [**dev@tooztech.com**](mailto:dev@tooztech.com).

## Contact

If you have any questions, don't hesitate to contact us at [**dev@tooztech.com**](mailto:dev@tooztech.com).

